package com.myrh.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myrh.domain.HttpResponse;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static com.myrh.constant.SecurityConstant.FORBIDDEN_MESSAGE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
/**
 * Point d'entrée d'authentification JWT pour les requêtes non autorisées. Cette classe est annotée avec
 * {@link org.springframework.stereotype.Component} pour être automatiquement détectée et gérée par Spring.
 * Elle étend {@link org.springframework.security.web.authentication.Http403ForbiddenEntryPoint} pour personnaliser la
 * réponse HTTP renvoyée en cas d'accès non autorisé (statut 403).
 *
 * Elle renvoie une réponse JSON contenant les détails de l'erreur au client.
 *
 */
@Component
public class JwtAuthenticationEntryPoint extends Http403ForbiddenEntryPoint {

    /**
     * Méthode appelée lorsqu'une exception d'authentification se produit (accès non autorisé).
     * Elle renvoie une réponse HTTP 403 avec un message d'erreur JSON.
     *
     * @param request    La requête HTTP.
     * @param response   La réponse HTTP.
     * @param exception  L'exception d'authentification.
     * @throws IOException Si une exception d'entrée/sortie survient.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
        HttpResponse httpResponse = new HttpResponse(FORBIDDEN.value(), FORBIDDEN, FORBIDDEN.getReasonPhrase().toUpperCase(), FORBIDDEN_MESSAGE);
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setStatus(FORBIDDEN.value());
        OutputStream outputStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, httpResponse);
        outputStream.flush();
    }
}
