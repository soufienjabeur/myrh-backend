package com.myrh.filter;

import com.myrh.utility.JWTTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Filtre d'autorisation JWT pour traiter les requêtes. Cette classe est annotée avec
 * {@link org.springframework.stereotype.Component} pour être automatiquement détectée et gérée par Spring.
 * Elle étend {@link org.springframework.web.filter.OncePerRequestFilter} pour garantir l'exécution du filtre une fois par requête.
 * Elle dépend de {@link com.myrh.security.JWTTokenProvider} pour la gestion des tokens JWT.
 *
 * @author Votre Nom
 * @version 1.0
 * @since 2024-02-19
 */
@Component
@RequiredArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final JWTTokenProvider jwtTokenProvider;

    /**
     * Constante représentant la méthode HTTP OPTIONS.
     */
    private static final String OPTIONS_HTTP_METHOD = "OPTIONS";

    /**
     * Entête d'autorisation pour le token JWT.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * Préfixe utilisé pour les tokens JWT dans l'entête d'autorisation.
     */
    private static final String TOKEN_PREFIX = "Bearer ";

    /**
     * Méthode appelée pour traiter chaque requête. Elle vérifie l'entête d'autorisation pour la présence
     * d'un token JWT, le valide, et configure l'authentification Spring Security si le token est valide.
     *
     * @param request       La requête HTTP.
     * @param response      La réponse HTTP.
     * @param filterChain   La chaîne des filtres à appeler.
     * @throws ServletException Si une exception de servlet survient.
     * @throws IOException      Si une exception d'entrée/sortie survient.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getMethod().equalsIgnoreCase(OPTIONS_HTTP_METHOD)) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            String authorizationHeader = request.getHeader(AUTHORIZATION);
            if (authorizationHeader == null || !authorizationHeader.startsWith(TOKEN_PREFIX)) {
                filterChain.doFilter(request, response);
                return;
            }
            String token = authorizationHeader.substring(TOKEN_PREFIX.length());
            String username = jwtTokenProvider.getSubject(token);
            if (jwtTokenProvider.isTokenValid(username, token) && SecurityContextHolder.getContext().getAuthentication() == null) {
                List<GrantedAuthority> authorities = jwtTokenProvider.getAuthorities(token);
                Authentication authentication = jwtTokenProvider.getAuthentication(username, authorities, request);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } else {
                SecurityContextHolder.clearContext();
            }
        }
        filterChain.doFilter(request, response);
    }
}
