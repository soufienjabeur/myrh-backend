package com.myrh.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myrh.domain.HttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static com.myrh.constant.SecurityConstant.ACCESS_DENIED_MESSAGE;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Gestionnaire des erreurs d'accès non autorisé pour les requêtes JWT. Cette classe est annotée avec
 * {@link org.springframework.stereotype.Component} pour être automatiquement détectée et gérée par Spring.
 * Elle implémente l'interface {@link org.springframework.security.web.access.AccessDeniedHandler} pour personnaliser la
 * réponse HTTP renvoyée en cas d'accès non autorisé (statut 401).
 * <p>
 * Elle renvoie une réponse JSON contenant les détails de l'erreur au client.
 */
@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * Méthode appelée lorsqu'une exception d'accès non autorisé se produit.
     * Elle renvoie une réponse HTTP 401 avec un message d'erreur JSON.
     *
     * @param request   La requête HTTP.
     * @param response  La réponse HTTP.
     * @param exception L'exception d'accès non autorisé.
     * @throws IOException Si une exception d'entrée/sortie survient.
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception) throws IOException {
        HttpResponse httpResponse = new HttpResponse(UNAUTHORIZED.value(), UNAUTHORIZED, UNAUTHORIZED.getReasonPhrase().toUpperCase(), ACCESS_DENIED_MESSAGE);
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setStatus(UNAUTHORIZED.value());
        OutputStream outputStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, httpResponse);
        outputStream.flush();
    }
}
