package com.myrh.controller;

import java.util.Date;
import java.util.List;

import com.myrh.domain.DemandeConge;
import com.myrh.service.VacationRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;


/**
 * Contrôleur pour la gestion des demandes de congé.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, {@code @RequiredArgsConstructor}, et {@code @DateTimeFormat}.
 */
@RestController
@RequestMapping(path = {"/demandeconge"})
@RequiredArgsConstructor
public class VacationRequestController {

    private final VacationRequestService vacationRequestService;

    /**
     * Gère la demande pour obtenir la liste de toutes les demandes de congé.
     *
     * @return ResponseEntity contenant la liste de toutes les demandes de congé, avec le code HTTP correspondant.
     */
    @GetMapping(value = "/list")
    public ResponseEntity<List<DemandeConge>> getAll() {
        return new ResponseEntity<>(vacationRequestService.getAll(), HttpStatus.OK);
    }

    /**
     * Gère la demande pour obtenir la liste de toutes les demandes de congé d'un employé.
     *
     * @param username Le nom d'utilisateur de l'employé.
     * @return ResponseEntity contenant la liste des demandes de congé de l'employé, avec le code HTTP correspondant.
     */
    @GetMapping(value = "/find/{username}")
    public ResponseEntity<List<DemandeConge>> getAllByEmployeeId(@PathVariable("username") String username) {
        return new ResponseEntity<>(vacationRequestService.getAllAskForVacationByEmployeeId(username), OK);
    }

    /**
     * Gère la demande d'ajout d'une nouvelle demande de congé.
     *
     * @param titre Le titre de la demande de congé.
     * @param cause La cause de la demande de congé.
     * @param dateDebut La date de début de la demande de congé au format MM-dd-yyyy.
     * @param dateFin La date de fin de la demande de congé au format MM-dd-yyyy.
     * @param username Le nom d'utilisateur associé à la demande de congé.
     * @return ResponseEntity contenant la nouvelle demande de congé ajoutée, avec le code HTTP correspondant.
     */
    @PostMapping("/add")
    public ResponseEntity<DemandeConge> saveItem(@RequestParam("titre") String titre, @RequestParam("cause") String cause,
                                                 @RequestParam("dateDebut") @DateTimeFormat(pattern = "MM-dd-yyyy") Date dateDebut,
                                                 @RequestParam("dateFin") @DateTimeFormat(pattern = "MM-dd-yyyy") Date dateFin,
                                                 @RequestParam("username") String username) {
        DemandeConge demandeConge = vacationRequestService.saveItem(titre, cause, dateDebut, dateFin, username);
        return new ResponseEntity<>(demandeConge, HttpStatus.OK);
    }

    /**
     * Gère la demande de mise à jour d'une demande de congé existante.
     *
     * @param currentTitre Le titre actuel de la demande de congé.
     * @param titre Le nouveau titre de la demande de congé.
     * @param cause La nouvelle cause de la demande de congé.
     * @param dateDebut La nouvelle date de début de la demande de congé au format MM-dd-yyyy.
     * @param dateFin La nouvelle date de fin de la demande de congé au format MM-dd-yyyy.
     * @param isAccepted La nouvelle valeur indiquant si la demande de congé est acceptée ou non.
     * @return ResponseEntity contenant la demande de congé mise à jour, avec le code HTTP correspondant.
     */
    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestParam("currentTitre") String currentTitre, @RequestParam("titre") String titre,
                                    @RequestParam("cause") String cause, @RequestParam("dateDebut") @DateTimeFormat(pattern = "MM-dd-yyyy") Date dateDebut,
                                    @RequestParam("dateFin") @DateTimeFormat(pattern = "MM-dd-yyyy") Date dateFin,
                                    @RequestParam("isAccepted") String isAccepted) {
        DemandeConge demandeConge = vacationRequestService.updateItem(currentTitre, titre, cause, dateDebut, dateFin, Boolean.parseBoolean(isAccepted));
        return new ResponseEntity<>(demandeConge, OK);
    }

    /**
     * Gère la demande pour obtenir une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé à récupérer.
     * @return ResponseEntity contenant la demande de congé demandée, avec le code HTTP correspondant.
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable long id) {
        try {
            DemandeConge DemandeConge = vacationRequestService.getById(id);
            return new ResponseEntity<>(DemandeConge, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("ERREUR INATTENDUE : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Gère la demande de suppression d'une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé à supprimer.
     * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            vacationRequestService.deleteById(id);
            return new ResponseEntity<>("DemandeConge SUPPRIMÉE AVEC SUCCÈS", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("IMPOSSIBLE DE SUPPRIMER DemandeConge", HttpStatus.BAD_REQUEST);
        }
    }
}
