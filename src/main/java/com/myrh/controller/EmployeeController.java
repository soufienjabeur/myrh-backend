package com.myrh.controller;

import com.myrh.domain.Employee;
import com.myrh.domain.HttpResponse;
import com.myrh.domain.UserPrincipal;
import com.myrh.exception.ExceptionHandling;
import com.myrh.exception.domain.EmailExistException;
import com.myrh.exception.domain.EmailNotFoundException;
import com.myrh.exception.domain.UserNotFoundException;
import com.myrh.exception.domain.UsernameExistException;
import com.myrh.service.EmployeeService;
import com.myrh.utility.JWTTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.myrh.constant.FileConstant.*;
import static com.myrh.constant.SecurityConstant.JWT_TOKEN_HEADER;
import static com.myrh.constant.UserImplConstant.EMAIL_SENT;
import static com.myrh.constant.UserImplConstant.USER_DELETED_SUCCESSFULLY;

/**
 * Contrôleur pour la gestion des employés.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, {@code @RequiredArgsConstructor}, et {@code @Autowired}.
 */
@RestController
@RequestMapping(path = { "/", "/employe" })
@RequiredArgsConstructor
public class EmployeeController extends ExceptionHandling {

	private final AuthenticationManager authenticationManager;
	private final EmployeeService employeeService;
	private final JWTTokenProvider jwtTokenProvider;


	/**
	 * Gère la demande de connexion d'un employé.
	 *
	 * @param user L'employé avec les informations d'identification.
	 * @return ResponseEntity contenant l'employé connecté et le jeton JWT, avec le code HTTP correspondant.
	 */
	@PostMapping("/login")
	public ResponseEntity<Employee> login(@RequestBody Employee user) {
		authenticate(user.getUsername(), user.getPassword());
		Employee loginUser = employeeService.findUserByUserName(user.getUsername());
		UserPrincipal userPrincipal = new UserPrincipal(loginUser);
		HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
		return new ResponseEntity<>(loginUser, jwtHeader, HttpStatus.OK);
	}

	/**
	 * Gère la demande d'enregistrement d'un nouvel employé.
	 *
	 * @param employee Le nouvel employé à enregistrer.
	 * @return ResponseEntity contenant le nouvel employé enregistré, avec le code HTTP correspondant.
	 * @throws UserNotFoundException Si l'utilisateur n'est pas trouvé.
	 * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
	 * @throws EmailExistException Si l'adresse e-mail existe déjà.
	 */
	@PostMapping("/register")
	public ResponseEntity<Employee> register(@RequestBody Employee employee) throws UserNotFoundException, UsernameExistException, EmailExistException {
		Employee newUser = employeeService.register(employee);
		return new ResponseEntity<>(newUser, HttpStatus.OK);
	}

	/**
	 * Gère la demande d'ajout d'un nouvel employé.
	 *
	 * @param firstName Le prénom du nouvel employé.
	 * @param lastName Le nom de famille du nouvel employé.
	 * @param username Le nom d'utilisateur du nouvel employé.
	 * @param email L'adresse e-mail du nouvel employé.
	 * @param role Le rôle du nouvel employé.
	 * @param isActive Indique si le nouvel employé est actif.
	 * @param isNotLocked Indique si le nouvel employé n'est pas verrouillé.
	 * @param profileImage L'image de profil du nouvel employé (optionnelle).
	 * @return ResponseEntity contenant le nouvel employé ajouté, avec le code HTTP correspondant.
	 * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
	 * @throws EmailExistException Si l'adresse e-mail existe déjà.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors du traitement de l'image de profil.
	 */
	@PostMapping("/add")
	public ResponseEntity<Employee> addNewUser(@RequestParam("firstName") String firstName,
											   @RequestParam("lastName") String lastName, @RequestParam("email") String username,
											   @RequestParam("username") String email, @RequestParam("role") String role,
											   @RequestParam("isActive") String isActive, @RequestParam("isNotLocked") String isNotLocked,
											   @RequestParam(value = "profileImage", required = false) MultipartFile profileImage)
			throws UsernameExistException, EmailExistException, IOException {
		Employee newUser = employeeService.addNewUser(firstName, lastName, username, email, role,
				Boolean.parseBoolean(isActive), Boolean.parseBoolean(isNotLocked), profileImage);
		return new ResponseEntity<>(newUser, HttpStatus.OK);
	}

	/**
	 * Gère la demande de mise à jour d'un employé existant.
	 *
	 * @param firstName Le nouveau prénom de l'employé.
	 * @param lastName Le nouveau nom de famille de l'employé.
	 * @param currentUsername L'actuel nom d'utilisateur de l'employé.
	 * @param username Le nouveau nom d'utilisateur de l'employé.
	 * @param email Le nouvel e-mail de l'employé.
	 * @param role Le nouveau rôle de l'employé.
	 * @param isActive Indique si l'employé est actif.
	 * @param isNotLocked Indique si l'employé n'est pas verrouillé.
	 * @param profileImage La nouvelle image de profil de l'employé (optionnelle).
	 * @return ResponseEntity contenant l'employé mis à jour, avec le code HTTP correspondant.
	 * @throws UsernameExistException Si le nouveau nom d'utilisateur existe déjà.
	 * @throws EmailExistException Si le nouvel e-mail existe déjà.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors du traitement de l'image de profil.
	 */
	@PostMapping("/update")
	public ResponseEntity<Employee> updateUser(@RequestParam("firstName") String firstName,
											   @RequestParam("lastName") String lastName, @RequestParam("currentUsername") String currentUsername,
											   @RequestParam("username") String username, @RequestParam("email") String email,
											   @RequestParam("role") String role, @RequestParam("isActive") String isActive,
											   @RequestParam("isNotLocked") String isNotLocked,
											   @RequestParam(value = "profileImage", required = false) MultipartFile profileImage)
			throws UsernameExistException, EmailExistException, IOException {
		Employee updateUser = employeeService.updateUser(currentUsername, firstName, lastName, username, email, role,
				Boolean.parseBoolean(isActive), Boolean.parseBoolean(isNotLocked), profileImage);
		return new ResponseEntity<>(updateUser, HttpStatus.OK);
	}

	/**
	 * Gère la demande pour obtenir un employé par son identifiant utilisateur.
	 *
	 * @param userId L'identifiant utilisateur de l'employé à récupérer.
	 * @return ResponseEntity contenant l'employé demandé, avec le code HTTP correspondant.
	 */
	@GetMapping("/find/{userId}")
	public ResponseEntity<Employee> getUser(@PathVariable("userId") String userId) {
		Employee user = employeeService.findUserByUserId(userId);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	/**
	 * Gère la demande pour obtenir la liste de tous les employés.
	 *
	 * @return ResponseEntity contenant la liste de tous les employés, avec le code HTTP correspondant.
	 */
	@GetMapping("/list")
	public ResponseEntity<List<Employee>> getAllUser() {
		List<Employee> users = employeeService.getUsers();
		return new ResponseEntity<>(users, HttpStatus.OK);
	}

	/**
	 * Gère la demande de réinitialisation du mot de passe pour une adresse e-mail donnée.
	 *
	 * @param email L'adresse e-mail pour laquelle réinitialiser le mot de passe.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 * @throws EmailNotFoundException Si l'adresse e-mail n'est pas trouvée.
	 */
	@GetMapping("/resetPassword/{email}")
	public ResponseEntity<HttpResponse> resetPassword(@PathVariable("email") String email) throws EmailNotFoundException {
		employeeService.resetPassword(email);
		return response(HttpStatus.OK, EMAIL_SENT + email);
	}

	/**
	 * Gère la demande de suppression d'un employé par son nom d'utilisateur.
	 *
	 * @param username Le nom d'utilisateur de l'employé à supprimer.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors de la suppression de l'image de profil.
	 */
	@DeleteMapping("/delete/{username}")
	public ResponseEntity<HttpResponse> deleteUser(@PathVariable("username") String username) throws IOException {
		employeeService.deleteUser(username);
		return response(HttpStatus.NO_CONTENT, USER_DELETED_SUCCESSFULLY);
	}

	/**
	 * Gère la demande de mise à jour de l'image de profil d'un employé.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 * @param profileImage La nouvelle image de profil de l'employé.
	 * @return ResponseEntity contenant l'employé mis à jour, avec le code HTTP correspondant.
	 * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
	 * @throws EmailExistException Si l'adresse e-mail existe déjà.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors du traitement de l'image de profil.
	 */
	@PostMapping("/updateProfileImage")
	public ResponseEntity<Employee> updateProfileImage(@RequestParam("username") String username,
													   @RequestParam(value = "profileImage") MultipartFile profileImage)
			throws UsernameExistException, EmailExistException, IOException {
		Employee user = employeeService.updateProfileImage(username, profileImage);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	/**
	 * Gère la demande d'obtenir l'image de profil d'un employé.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 * @param fileName Le nom de fichier de l'image de profil.
	 * @return Tableau d'octets contenant les données de l'image de profil, avec le type de contenu approprié.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors de la récupération de l'image de profil.
	 */
	@GetMapping(path = "/image/{username}/{fileName}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getProfileImage(@PathVariable("username") String username, @PathVariable("fileName") String fileName)
			throws IOException {
		return Files.readAllBytes(Paths.get(USER_FOLDER + username + FORWARD_SLASH + fileName));
	}

	/**
	 * Gère la demande d'obtenir une image de profil temporaire pour un employé.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 * @return Tableau d'octets contenant les données de l'image de profil temporaire, avec le type de contenu approprié.
	 * @throws IOException En cas d'erreur d'entrée/sortie lors de la récupération de l'image de profil temporaire.
	 */
	@GetMapping(path = "/image/profile/{username}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getTempProfileImage(@PathVariable("username") String username) throws IOException {
		URL url = new URL(TEMP_PROFILE_IMAGE_BASE_URL + username);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try (InputStream inputStream = url.openStream()) {
			int byteRead;
			byte[] chunk = new byte[1024];
			while ((byteRead = inputStream.read(chunk)) > 0) {
				byteArrayOutputStream.write(chunk, 0, byteRead);
			}
		}
		return byteArrayOutputStream.toByteArray();
	}

	/**
	 * Crée une ResponseEntity contenant un objet HttpResponse avec le code HTTP et le message spécifiés.
	 *
	 * @param httpStatus Le code HTTP à inclure dans la réponse.
	 * @param message Le message à inclure dans la réponse.
	 * @return ResponseEntity contenant un objet HttpResponse.
	 */
	private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
		HttpResponse body = new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase());
		return new ResponseEntity<>(body, httpStatus);
	}

	/**
	 * Obtient les en-têtes HTTP avec le jeton JWT pour l'utilisateur donné.
	 *
	 * @param user L'utilisateur pour lequel générer le jeton JWT.
	 * @return HttpHeaders contenant le jeton JWT dans l'en-tête.
	 */
	private HttpHeaders getJwtHeader(UserPrincipal user) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(user));
		return headers;
	}

	/**
	 * Authentifie un utilisateur avec le nom d'utilisateur et le mot de passe donnés.
	 *
	 * @param username Le nom d'utilisateur de l'utilisateur à authentifier.
	 * @param password Le mot de passe de l'utilisateur à authentifier.
	 */
	private void authenticate(String username, String password) {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	}
}
