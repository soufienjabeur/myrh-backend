package com.myrh.controller;

import java.util.List;

import com.myrh.domain.DemandeRecrutement;
import com.myrh.domain.OpportuniteRecrutement;
import com.myrh.service.RecruitmentOpportunityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;


/**
 * Contrôleur pour la gestion des opportunités de recrutement.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, et {@code @RequiredArgsConstructor}.
 */
@RestController
@RequestMapping(path = { "/opportuniterecrutement" })
@RequiredArgsConstructor
public class RecruitmentOpportunityController {

	private final RecruitmentOpportunityService recruitmentOpportunityService;

	/**
	 * Récupère la liste de toutes les opportunités de recrutement.
	 *
	 * @return ResponseEntity contenant la liste des opportunités de recrutement et le code HTTP correspondant.
	 */
	@GetMapping("/list")
	public ResponseEntity<List<OpportuniteRecrutement>> getAll() {
		return new ResponseEntity<>(recruitmentOpportunityService.getAll(), HttpStatus.OK);
	}

	/**
	 * Ajoute une nouvelle opportunité de recrutement avec le titre et la description fournis.
	 *
	 * @param titre Le titre de l'opportunité de recrutement.
	 * @param description La description de l'opportunité de recrutement.
	 * @return ResponseEntity contenant la nouvelle opportunité de recrutement ajoutée et le code HTTP correspondant.
	 */
	@PostMapping("/add")
	public ResponseEntity<OpportuniteRecrutement> saveItem(@RequestParam("titre") String titre,
														   @RequestParam("description") String description) {
		OpportuniteRecrutement opportuniteRecrutement = recruitmentOpportunityService.saveItem(titre, description);
		return new ResponseEntity<>(opportuniteRecrutement, HttpStatus.OK);
	}

	/**
	 * Met à jour une opportunité de recrutement existante avec les nouvelles informations fournies.
	 *
	 * @param currentTitre Le titre actuel de l'opportunité de recrutement.
	 * @param titre Le nouveau titre de l'opportunité de recrutement.
	 * @param description La nouvelle description de l'opportunité de recrutement.
	 * @return ResponseEntity contenant l'opportunité de recrutement mise à jour et le code HTTP correspondant.
	 */
	@PostMapping("/update")
	public ResponseEntity<OpportuniteRecrutement> update(@RequestParam("currentTitre") String currentTitre,
														 @RequestParam("titre") String titre, @RequestParam("description") String description) {
		OpportuniteRecrutement opportuniteRecrutement = recruitmentOpportunityService.updateItem(currentTitre, titre,
				description);
		return new ResponseEntity<>(opportuniteRecrutement, HttpStatus.OK);
	}

	/**
	 * Récupère la liste des demandes de recrutement associées à une opportunité spécifique.
	 *
	 * @param titre Le titre de l'opportunité de recrutement pour laquelle récupérer les demandes.
	 * @return ResponseEntity contenant la liste des demandes de recrutement et le code HTTP correspondant.
	 */
	@GetMapping("/demandes/{titre}")
	public ResponseEntity<List<DemandeRecrutement>> getDemandeRecrutements(@PathVariable("titre") String titre) {
		List<DemandeRecrutement> demandes = recruitmentOpportunityService.getDemandeRecrutements(titre);
		return new ResponseEntity<>(demandes, HttpStatus.OK);
	}

	/**
	 * Récupère une opportunité de recrutement spécifique en fonction de son identifiant.
	 *
	 * @param id L'identifiant de l'opportunité de recrutement à récupérer.
	 * @return ResponseEntity contenant l'opportunité de recrutement demandée et le code HTTP correspondant.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		try {
			OpportuniteRecrutement opportuniteRecrutement = recruitmentOpportunityService.getById(id);
			return new ResponseEntity<>(opportuniteRecrutement, HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("UNE ERREUR INATTENDUE S'EST PRODUITE : " + ex.getMessage(),
					HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Supprime une opportunité de recrutement en fonction de son identifiant.
	 *
	 * @param id L'identifiant de l'opportunité de recrutement à supprimer.
	 * @return ResponseEntity avec un message indiquant le succès ou l'échec de la suppression et le code HTTP correspondant.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			recruitmentOpportunityService.deeleteById(id);
			return new ResponseEntity<>("OpportuniteRecrutement SUPPRIMÉE AVEC SUCCÈS", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("IMPOSSIBLE DE SUPPRIMER L'OpportuniteRecrutement", HttpStatus.BAD_REQUEST);
		}
	}
}
