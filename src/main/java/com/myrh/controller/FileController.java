package com.myrh.controller;

import com.myrh.domain.Document;
import com.myrh.domain.HttpResponse;
import com.myrh.service.DocumentService;
import com.myrh.service.ProjectService;
import com.myrh.service.FileSystemStorageService;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.http.HttpStatus.NO_CONTENT;


/**
 * Contrôleur pour la gestion des fichiers et des documents.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, et {@code @RequiredArgsConstructor}.
 */
@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileController {

    private final FileSystemStorageService fileSytemStorage;

    private final DocumentService documentservice;

    private final ProjectService projectService;

    /**
     * Télécharge un fichier unique associé à un projet spécifique.
     *
     * @param name Le nom du fichier.
     * @param file Le fichier à télécharger.
     * @param titreProjet Le titre du projet auquel le fichier est associé.
     * @return ResponseEntity contenant le document associé au fichier téléchargé.
     */
    @PostMapping("/upload")
    public ResponseEntity<Document> uploadSingleFile(@RequestParam("name") String name,
                                                     @RequestParam("file") MultipartFile file, @RequestParam("titreProjet") String titreProjet) {
        Document document = documentservice.saveItem(name, file, titreProjet);
        return ResponseEntity.ok(document);
    }

    /**
     * Supprime un document en fonction de son identifiant.
     *
     * @param id L'identifiant du document à supprimer.
     * @return ResponseEntity contenant un message indiquant le succès de la suppression.
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpResponse> deleteDoc(@PathVariable Long id) {
        documentservice.deleteById(id);
        return response(NO_CONTENT, "DOCUMENT SUPPRIMÉ AVEC SUCCÈS");
    }

    /**
     * Crée une ResponseEntity contenant un objet HttpResponse avec le code HTTP et le message spécifiés.
     *
     * @param httpStatus Le code HTTP à inclure dans la réponse.
     * @param message Le message à inclure dans la réponse.
     * @return ResponseEntity contenant un objet HttpResponse.
     */
    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        HttpResponse body = new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase());
        return new ResponseEntity<>(body, httpStatus);
    }
}
