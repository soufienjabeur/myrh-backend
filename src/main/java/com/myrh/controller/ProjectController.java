package com.myrh.controller;

import com.myrh.domain.Document;
import com.myrh.domain.Projet;
import com.myrh.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Contrôleur pour la gestion des opérations liées aux projets.
 * <p>
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, et {@code @RequiredArgsConstructor}.
 */
@RestController
@RequestMapping(value = "/projet")
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    /**
     * Récupère la liste de tous les projets.
     *
     * @return ResponseEntity contenant la liste des projets et le code HTTP correspondant.
     */
    @GetMapping(value = "/list")
    public ResponseEntity<List<Projet>> getAll() {
        return new ResponseEntity<>(projectService.getAll(), HttpStatus.OK);
    }

    /**
     * Ajoute un nouveau projet avec le titre et la description fournis.
     *
     * @param titre       Le titre du projet.
     * @param description La description du projet.
     * @return ResponseEntity contenant le nouveau projet ajouté et le code HTTP correspondant.
     */
    @PostMapping("/add")
    public ResponseEntity<?> saveItem(@RequestParam("titre") String titre, @RequestParam("description") String description) {
        Projet projet = projectService.saveItem(titre, description);
        return new ResponseEntity<>(projet, HttpStatus.OK);
    }

    /**
     * Met à jour un projet existant avec les nouvelles informations fournies.
     *
     * @param currentTitre Le titre actuel du projet.
     * @param titre        Le nouveau titre du projet.
     * @param description  La nouvelle description du projet.
     * @return ResponseEntity contenant le projet mis à jour et le code HTTP correspondant.
     */
    @PostMapping("/update")
    public ResponseEntity<Projet> update(@RequestParam("currentTitre") String currentTitre, @RequestParam("titre") String titre, @RequestParam("description") String description) {
        Projet projet = projectService.updateItem(currentTitre, titre, description);
        return new ResponseEntity<>(projet, HttpStatus.OK);
    }

    /**
     * Récupère la liste des documents associés à un projet spécifique.
     *
     * @param titreProjet Le titre du projet pour lequel récupérer les documents.
     * @return ResponseEntity contenant la liste des documents et le code HTTP correspondant.
     */
    @GetMapping("/documents/{titreProjet}")
    public ResponseEntity<List<Document>> getDocumentsByTitreProjet(@PathVariable("titreProjet") String titreProjet) {
        try {
            List<Document> documents = projectService.getDocumentsByTitreProjet(titreProjet);
            return ResponseEntity.ok(documents);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Récupère un projet spécifique en fonction de son identifiant.
     *
     * @param id L'identifiant du projet à récupérer.
     * @return ResponseEntity contenant le projet demandé et le code HTTP correspondant.
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable long id) {
        try {
            Projet projet = projectService.getById(id);
            return new ResponseEntity<>(projet, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("UNE ERREUR INATTENDUE S'EST PRODUITE : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Supprime un projet en fonction de son identifiant.
     *
     * @param id L'identifiant du projet à supprimer.
     * @return ResponseEntity avec un message indiquant le succès ou l'échec de la suppression et le code HTTP correspondant.
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            projectService.deeleteById(id);
            return new ResponseEntity<>("Projet SUPPRIMÉ AVEC SUCCÈS", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("IMPOSSIBLE DE SUPPRIMER LE Projet", HttpStatus.BAD_REQUEST);
        }
    }
}

