package com.myrh.controller;

import java.util.Date;
import java.util.List;

import com.myrh.domain.Formation;
import com.myrh.service.FormationService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;


/**
 * Contrôleur pour la gestion des formations.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, et {@code @RequiredArgsConstructor}.
 */
@RestController
@RequestMapping(path = { "/formation" })
@RequiredArgsConstructor
public class FormationController {

	private final FormationService formationService;

	/**
	 * Récupère la liste de toutes les formations.
	 *
	 * @return ResponseEntity contenant la liste des formations et le code HTTP correspondant.
	 */
	@GetMapping(value = "/list")
	public ResponseEntity<List<Formation>> getAll() {
		return new ResponseEntity<>(formationService.getAll(), HttpStatus.OK);
	}

	/**
	 * Ajoute une nouvelle formation avec le titre, la date de début et la date de fin fournis.
	 *
	 * @param titre Le titre de la formation.
	 * @param dateDebut La date de début de la formation au format "yyyy-MM-dd".
	 * @param dateFin La date de fin de la formation au format "yyyy-MM-dd".
	 * @return ResponseEntity contenant la nouvelle formation ajoutée et le code HTTP correspondant.
	 */
	@PostMapping("/add")
	public ResponseEntity<Formation> saveItem(@RequestParam("titre") String titre,
											  @RequestParam("dateDebut") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateDebut,
											  @RequestParam("dateFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFin) {
		Formation formation = formationService.saveItem(titre, dateDebut, dateFin);
		return new ResponseEntity<>(formation, HttpStatus.OK);
	}

	/**
	 * Met à jour une formation existante avec les nouvelles informations fournies.
	 *
	 * @param currentTitre Le titre actuel de la formation.
	 * @param titre Le nouveau titre de la formation.
	 * @param dateDebut La nouvelle date de début de la formation au format "yyyy-MM-dd".
	 * @param dateFin La nouvelle date de fin de la formation au format "yyyy-MM-dd".
	 * @return ResponseEntity contenant la formation mise à jour et le code HTTP correspondant.
	 */
	@PostMapping("/update")
	public ResponseEntity<?> update(@RequestParam("currentTitre") String currentTitre,
									@RequestParam("titre") String titre, @RequestParam("dateDebut") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateDebut,
									@RequestParam("dateFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFin) {
		Formation formation = formationService.updateItem(currentTitre, titre, dateDebut, dateFin);
		return new ResponseEntity<>(formation, HttpStatus.OK);
	}

	/**
	 * Récupère une formation spécifique en fonction de son identifiant.
	 *
	 * @param id L'identifiant de la formation à récupérer.
	 * @return ResponseEntity contenant la formation demandée et le code HTTP correspondant.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		try {
			Formation formation = formationService.getById(id);
			return new ResponseEntity<>(formation, HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("UNE ERREUR INATTENDUE S'EST PRODUITE : " + ex.getMessage(),
					HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Supprime une formation en fonction de son identifiant.
	 *
	 * @param id L'identifiant de la formation à supprimer.
	 * @return ResponseEntity avec un message indiquant le succès ou l'échec de la suppression et le code HTTP correspondant.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			formationService.deleteById(id);
			return new ResponseEntity<>("Formation SUPPRIMÉE AVEC SUCCÈS", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("IMPOSSIBLE DE SUPPRIMER LA Formation", HttpStatus.BAD_REQUEST);
		}
	}
}
