package com.myrh.controller;

import com.myrh.domain.Departement;
import com.myrh.domain.Employee;
import com.myrh.domain.HttpResponse;
import com.myrh.exception.domain.UserNotFoundException;
import com.myrh.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;


/**
 * Contrôleur pour la gestion des départements.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, {@code @RequiredArgsConstructor}, et {@code @Autowired}.
 */
@RestController
@RequestMapping("/departement")
@RequiredArgsConstructor
public class DepartmentController {

	private final DepartmentService departmentService;

	/**
	 * Gère la demande pour obtenir la liste de tous les départements.
	 *
	 * @return ResponseEntity contenant la liste de tous les départements, avec le code HTTP correspondant.
	 */
	@GetMapping(value = "/list")
	public ResponseEntity<List<Departement>> getAll() {
		return new ResponseEntity<>(departmentService.getAll(), HttpStatus.OK);
	}

	/**
	 * Gère la demande d'ajout d'un nouveau département.
	 *
	 * @param name Le nom du nouveau département.
	 * @return ResponseEntity contenant le nouveau département ajouté, avec le code HTTP correspondant.
	 */
	@PostMapping("/add")
	public ResponseEntity<Departement> saveItem(@RequestParam("name") String name) {
		Departement departement = departmentService.saveItem(name);
		return new ResponseEntity<>(departement, HttpStatus.OK);
	}

	/**
	 * Gère la demande de mise à jour d'un département existant.
	 *
	 * @param currentName Le nom actuel du département.
	 * @param name Le nouveau nom du département.
	 * @return ResponseEntity contenant le département mis à jour, avec le code HTTP correspondant.
	 */
	@PostMapping("/update")
	public ResponseEntity<?> update(@RequestParam("currentName") String currentName, @RequestParam("name") String name) {
		Departement departement = departmentService.updateItem(currentName, name);
		return new ResponseEntity<>(departement, HttpStatus.OK);
	}

	/**
	 * Gère la demande pour obtenir un département par son identifiant.
	 *
	 * @param id L'identifiant du département à récupérer.
	 * @return ResponseEntity contenant le département demandé, avec le code HTTP correspondant.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		try {
			Departement departement = departmentService.getById(id);
			return new ResponseEntity<>(departement, HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("ERREUR INATTENDUE : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Gère la demande de suppression d'un département par son identifiant.
	 *
	 * @param id L'identifiant du département à supprimer.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		departmentService.deleteById(id);
		return new ResponseEntity<>("Département SUPPRIMÉ AVEC SUCCÈS", HttpStatus.OK);
	}

	/**
	 * Gère la demande d'assignation d'un employé à un département.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 * @param departmentId L'identifiant du département.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 * @throws UserNotFoundException Si l'utilisateur n'est pas trouvé.
	 */
	@PostMapping("/assign")
	public ResponseEntity<String> assignEmployeeToDepartment(@RequestParam("username") String username,
															 @RequestParam("departmentId") Long departmentId) throws UserNotFoundException {
		departmentService.assignEmployeeToDepartment(username, departmentId);
		return ResponseEntity.ok("Employé assigné au département avec succès.");
	}

	/**
	 * Gère la demande pour obtenir la liste des membres d'un département.
	 *
	 * @param id L'identifiant du département.
	 * @return ResponseEntity contenant la liste des employés du département, avec le code HTTP correspondant.
	 */
	@GetMapping("/employees/{id}")
	public ResponseEntity<List<Employee>> getDepMembers(@PathVariable("id") Long id) {
		List<Employee> employees = departmentService.getDepMembers(id);
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	/**
	 * Gère la demande de suppression d'un employé d'un département.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 */
	@DeleteMapping("/removeEmployeFromDep/{username}")
	public ResponseEntity<HttpResponse> removeEmployeFromDep(@PathVariable("username") String username) {
		departmentService.removeEmplFromDep(username);
		return response(HttpStatus.OK, "Employé supprimé avec succès du département.");
	}

	/**
	 * Crée une ResponseEntity contenant un objet HttpResponse avec le code HTTP et le message spécifiés.
	 *
	 * @param httpStatus Le code HTTP à inclure dans la réponse.
	 * @param message Le message à inclure dans la réponse.
	 * @return ResponseEntity contenant un objet HttpResponse.
	 */
	private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
		HttpResponse body = new HttpResponse(httpStatus.value(), httpStatus,
				httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase());
		return new ResponseEntity<>(body, httpStatus);
	}
}

    
