package com.myrh.controller;

import java.util.List;

import com.myrh.domain.DemandeRecrutement;
import com.myrh.service.RecruitmentRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.http.HttpStatus.OK;


/**
 * Contrôleur pour la gestion des demandes de recrutement.
 *
 * Ce contrôleur utilise les annotations Spring telles que {@code @RestController},
 * {@code @RequestMapping}, {@code @RequiredArgsConstructor}, et {@code @Autowired}.
 */
@RestController
@RequestMapping("/demanderecrutement")
@RequiredArgsConstructor
public class RecruitmentRequestController {

	private final RecruitmentRequestService recruitmentRequestService;

	/**
	 * Gère la demande pour obtenir la liste de toutes les demandes de recrutement.
	 *
	 * @return ResponseEntity contenant la liste de toutes les demandes de recrutement, avec le code HTTP correspondant.
	 */
	@GetMapping(value = "/list")
	public ResponseEntity<List<DemandeRecrutement>> getAll() {
		return new ResponseEntity<>(recruitmentRequestService.getAll(), HttpStatus.OK);
	}

	/**
	 * Gère la demande d'ajout d'une nouvelle demande de recrutement.
	 *
	 * @param message Le message de la demande de recrutement.
	 * @param titreOpportunite Le titre de l'opportunité de recrutement.
	 * @param cvFile Le fichier CV joint à la demande de recrutement (optionnel).
	 * @param username Le nom d'utilisateur associé à la demande de recrutement.
	 * @return ResponseEntity contenant la nouvelle demande de recrutement ajoutée, avec le code HTTP correspondant.
	 */
	@PostMapping("/add")
	public ResponseEntity<DemandeRecrutement> saveItem1(@RequestParam("message") String message,
														@RequestParam("titreOpportunite") String titreOpportunite,
														@RequestParam(value = "cvFile", required = false) MultipartFile cvFile,
														@RequestParam("username") String username) {
		DemandeRecrutement demandeRecrutement = recruitmentRequestService.saveItem1(message, titreOpportunite, cvFile, username);
		return ResponseEntity.ok(demandeRecrutement);
	}

	/**
	 * Gère la demande de mise à jour d'une demande de recrutement existante.
	 *
	 * @param currentMessage Le message actuel de la demande de recrutement.
	 * @param message Le nouveau message de la demande de recrutement.
	 * @param cvFile Le nouveau fichier CV joint à la demande de recrutement (optionnel).
	 * @return ResponseEntity contenant la demande de recrutement mise à jour, avec le code HTTP correspondant.
	 */
	@PostMapping("/update")
	public ResponseEntity<DemandeRecrutement> update(@RequestParam("currentMessage") String currentMessage,
													 @RequestParam("message") String message,
													 @RequestParam(value = "cvFile", required = false) MultipartFile cvFile) {
		DemandeRecrutement demandeRecrutement = recruitmentRequestService.updateItem(currentMessage, message, cvFile);
		return new ResponseEntity<>(demandeRecrutement, OK);
	}

	/**
	 * Gère la demande pour obtenir une demande de recrutement par son identifiant.
	 *
	 * @param id L'identifiant de la demande de recrutement à récupérer.
	 * @return ResponseEntity contenant la demande de recrutement demandée, avec le code HTTP correspondant.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		try {
			DemandeRecrutement DemandeRecrutement = recruitmentRequestService.getById(id);
			return new ResponseEntity<>(DemandeRecrutement, HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("ERREUR INATTENDUE : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Gère la demande de suppression d'une demande de recrutement par son identifiant.
	 *
	 * @param id L'identifiant de la demande de recrutement à supprimer.
	 * @return ResponseEntity contenant un message de réussite, avec le code HTTP correspondant.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			recruitmentRequestService.deeleteById(id);
			return new ResponseEntity<>("DemandeRecrutement SUPPRIMÉE AVEC SUCCÈS", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("IMPOSSIBLE DE SUPPRIMER DemandeRecrutement", HttpStatus.BAD_REQUEST);
		}
	}
}
