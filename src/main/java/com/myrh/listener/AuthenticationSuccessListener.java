package com.myrh.listener;

import java.util.concurrent.ExecutionException;

import com.myrh.service.LoginAttemptService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.myrh.domain.UserPrincipal;

/**
 * Classe écoutant les succès d'authentification et gérant les actions associées.
 */
@Component
@RequiredArgsConstructor
public class AuthenticationSuccessListener {

	private final LoginAttemptService loginAttemptService;

	/**
	 * Écouteur d'événement pour les succès d'authentification.
	 *
	 * @param event L'événement de succès d'authentification.
	 * @throws ExecutionException Si une exception d'exécution se produit lors de la gestion de l'événement.
	 */
	@EventListener
	public void onAuthenticationSuccess(AuthenticationSuccessEvent event) throws ExecutionException {
		Object principal = event.getAuthentication().getPrincipal();
		if (principal instanceof UserPrincipal) {
			UserPrincipal user = (UserPrincipal) event.getAuthentication().getPrincipal();
			loginAttemptService.removeUserFromLoginAttemptCache(user.getUsername());
		}
	}
}

