package com.myrh.listener;

import java.util.concurrent.ExecutionException;

import com.myrh.service.LoginAttemptService;
import lombok.AllArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;


/**
 * Classe écoutant les échecs d'authentification et gérant les actions associées.
 */
@Component
@AllArgsConstructor
public class AuthenticationFailureListener {

	private final LoginAttemptService loginAttemptService;

	/**
	 * Écouteur d'événement pour les échecs d'authentification par des informations d'identification incorrectes.
	 *
	 * @param event L'événement d'échec d'authentification.
	 * @throws ExecutionException Si une exception d'exécution se produit lors de la gestion de l'événement.
	 */
	@EventListener
	public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent event) throws ExecutionException {
		Object principal = event.getAuthentication().getPrincipal();
		if (principal instanceof String) {
			String username = (String) event.getAuthentication().getPrincipal();
			loginAttemptService.addUserToLoginAttemptCache(username);
		}
	}
}

