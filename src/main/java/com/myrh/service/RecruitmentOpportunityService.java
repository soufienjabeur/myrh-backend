package com.myrh.service;

import com.myrh.domain.DemandeRecrutement;
import com.myrh.domain.OpportuniteRecrutement;

import java.util.List;

public interface RecruitmentOpportunityService {

    /**
     * Récupère toutes les opportunités de recrutement.
     *
     * @return La liste de toutes les opportunités de recrutement.
     */
    List<OpportuniteRecrutement> getAll();

    /**
     * Récupère une opportunité de recrutement par son identifiant.
     *
     * @param id L'identifiant de l'opportunité de recrutement.
     * @return L'opportunité de recrutement correspondant à l'identifiant.
     */
    OpportuniteRecrutement getById(long id);

    /**
     * Ajoute une nouvelle opportunité de recrutement.
     *
     * @param titre      Le titre de l'opportunité de recrutement.
     * @param description La description de l'opportunité de recrutement.
     * @return L'opportunité de recrutement ajoutée.
     */
    OpportuniteRecrutement saveItem(String titre, String description);

    /**
     * Met à jour une opportunité de recrutement existante.
     *
     * @param currentTitre L'ancien titre de l'opportunité de recrutement.
     * @param titre         Le nouveau titre de l'opportunité de recrutement.
     * @param description   La nouvelle description de l'opportunité de recrutement.
     * @return L'opportunité de recrutement mise à jour.
     */
    OpportuniteRecrutement updateItem(String currentTitre, String titre, String description);

    /**
     * Récupère la liste des demandes de recrutement associées à une opportunité de recrutement par son titre.
     *
     * @param titre Le titre de l'opportunité de recrutement.
     * @return La liste des demandes de recrutement associées à l'opportunité de recrutement.
     */
    List<DemandeRecrutement> getDemandeRecrutements(String titre);

    /**
     * Supprime une opportunité de recrutement par son identifiant.
     *
     * @param id L'identifiant de l'opportunité de recrutement à supprimer.
     */
    void deeleteById(long id);
}
