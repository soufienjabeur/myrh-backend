package com.myrh.service;

import com.myrh.domain.Formation;

import java.util.Date;
import java.util.List;

/**
 * Service de gestion des formations.
 */
public interface FormationService {

    /**
     * Ajoute une nouvelle formation.
     *
     * @param titre    Le titre de la formation.
     * @param dateDeb  La date de début de la formation.
     * @param dateFin  La date de fin de la formation.
     * @return La formation ajoutée.
     */
    Formation saveItem(String titre, Date dateDeb, Date dateFin);

    /**
     * Met à jour une formation existante.
     *
     * @param currentTitre Le titre actuel de la formation.
     * @param titre        Le nouveau titre de la formation.
     * @param dateDeb      La nouvelle date de début de la formation.
     * @param dateFin      La nouvelle date de fin de la formation.
     * @return La formation mise à jour.
     */
    Formation updateItem(String currentTitre, String titre, Date dateDeb, Date dateFin);

    /**
     * Récupère toutes les formations.
     *
     * @return La liste de toutes les formations.
     */
    List<Formation> getAll();

    /**
     * Récupère une formation par son identifiant.
     *
     * @param id L'identifiant de la formation.
     * @return La formation correspondant à l'identifiant.
     */
    Formation getById(long id);

    /**
     * Supprime une formation par son identifiant.
     *
     * @param id L'identifiant de la formation à supprimer.
     */
    void deleteById(long id);
}
