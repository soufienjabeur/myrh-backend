package com.myrh.service;

import com.myrh.domain.DemandeRecrutement;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Service interface for managing recruitment requests.
 */
public interface RecruitmentRequestService {

    /**
     * Retrieves all recruitment requests.
     *
     * @return List of all recruitment requests.
     */
    List<DemandeRecrutement> getAll();

    /**
     * Retrieves a recruitment request by its ID.
     *
     * @param id The ID of the recruitment request.
     * @return The recruitment request with the specified ID.
     */
    DemandeRecrutement getById(long id);

    /**
     * Saves a new recruitment request.
     *
     * @param message          The message associated with the recruitment request.
     * @param titreOpportunite The title of the recruitment opportunity.
     * @param cvFile           The CV file attached to the recruitment request.
     * @param username         The username of the employee associated with the request.
     * @return The saved recruitment request.
     */
    DemandeRecrutement saveItem1(String message, String titreOpportunite, MultipartFile cvFile, String username);

    /**
     * Updates an existing recruitment request.
     *
     * @param currentMessage The current message of the recruitment request to be updated.
     * @param message        The new message for the recruitment request.
     * @param cvFile         The new CV file attached to the recruitment request.
     * @return The updated recruitment request.
     */
    DemandeRecrutement updateItem(String currentMessage, String message, MultipartFile cvFile);

    /**
     * Deletes a recruitment request by its ID.
     *
     * @param id The ID of the recruitment request to be deleted.
     */
    void deeleteById(long id);
}
