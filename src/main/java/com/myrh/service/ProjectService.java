package com.myrh.service;

import com.myrh.domain.Document;
import com.myrh.domain.Projet;

import java.util.List;

public interface ProjectService {

    /**
     * Récupère tous les projets.
     *
     * @return La liste de tous les projets.
     */
    List<Projet> getAll();

    /**
     * Récupère un projet par son identifiant.
     *
     * @param id L'identifiant du projet.
     * @return Le projet correspondant à l'identifiant.
     */
    Projet getById(long id);

    /**
     * Ajoute un nouveau projet.
     *
     * @param titre   Le titre du projet.
     * @param descrip La description du projet.
     * @return Le projet ajouté.
     */
    Projet saveItem(String titre, String descrip);

    /**
     * Met à jour un projet existant.
     *
     * @param currentTitre L'ancien titre du projet.
     * @param titre        Le nouveau titre du projet.
     * @param descrip      La nouvelle description du projet.
     * @return Le projet mis à jour.
     */
    Projet updateItem(String currentTitre, String titre, String descrip);

    /**
     * Récupère la liste des documents associés à un projet par son titre.
     *
     * @param titreProjet Le titre du projet.
     * @return La liste des documents associés au projet.
     * @throws IllegalArgumentException Si le projet n'est pas trouvé avec le titre spécifié.
     */
    List<Document> getDocumentsByTitreProjet(String titreProjet);

    /**
     * Supprime un projet par son identifiant.
     *
     * @param id L'identifiant du projet à supprimer.
     */
    void deeleteById(long id);

}

