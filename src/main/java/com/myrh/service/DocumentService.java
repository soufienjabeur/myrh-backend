package com.myrh.service;

import com.myrh.domain.Document;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Interface pour le service de gestion des documents.
 */
public interface DocumentService {

    /**
     * Enregistre un document avec le nom, le fichier et le titre du projet spécifiés.
     *
     * @param name         Le nom du document.
     * @param documentUrl  Le fichier du document.
     * @param titreProjet  Le titre du projet auquel le document est associé.
     * @return Le document enregistré.
     */
    Document saveItem(String name, MultipartFile documentUrl, String titreProjet);

    /**
     * Récupère tous les documents.
     *
     * @return La liste de tous les documents.
     */
    List<Document> getAll();

    /**
     * Récupère un document par son identifiant.
     *
     * @param id L'identifiant du document.
     * @return Le document correspondant à l'identifiant, ou {@code null} s'il n'est pas trouvé.
     */
    Document getById(long id);

    /**
     * Met à jour les informations d'un document.
     *
     * @param document Le document avec les nouvelles informations.
     * @return Le document mis à jour.
     */
    Document updateItem(Document document);

    /**
     * Supprime un document par son identifiant.
     *
     * @param id L'identifiant du document à supprimer.
     */
    void deleteById(long id);
}

