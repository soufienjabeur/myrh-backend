package com.myrh.service;

import java.util.concurrent.ExecutionException;

/**
 * Service de gestion des tentatives de connexion.
 */
public interface LoginAttemptService {

    /**
     * Supprime un utilisateur du cache des tentatives de connexion.
     *
     * @param username Le nom d'utilisateur à supprimer du cache.
     */
    void removeUserFromLoginAttemptCache(String username);

    /**
     * Ajoute un utilisateur au cache des tentatives de connexion.
     *
     * @param username Le nom d'utilisateur à ajouter au cache.
     */
    void addUserToLoginAttemptCache(String username);

    /**
     * Vérifie si un utilisateur a dépassé le nombre maximal de tentatives autorisées.
     *
     * @param username Le nom d'utilisateur à vérifier.
     * @return Vrai s'il a dépassé le nombre maximal de tentatives, sinon faux.
     */
    Boolean hasExceededMaxAttempts(String username) throws ExecutionException;
}
