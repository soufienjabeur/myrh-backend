package com.myrh.service;

import com.myrh.domain.DemandeConge;

import java.util.Date;
import java.util.List;

/**
 * Interface du service pour la gestion des demandes de congé.
 * Fournit des méthodes pour créer, mettre à jour, récupérer et supprimer des demandes de congé.
 */
public interface VacationRequestService {

    /**
     * Enregistre une nouvelle demande de congé.
     *
     * @param titre    Le titre de la demande de congé.
     * @param cause    La cause de la demande de congé.
     * @param dateDeb  La date de début de la demande de congé.
     * @param dateFin  La date de fin de la demande de congé.
     * @param username Le nom d'utilisateur de l'employé effectuant la demande.
     * @return La demande de congé enregistrée.
     */
    DemandeConge saveItem(String titre, String cause, Date dateDeb, Date dateFin, String username);

    /**
     * Met à jour une demande de congé existante.
     *
     * @param currentTitre Le titre actuel de la demande de congé.
     * @param titre        Le nouveau titre de la demande de congé.
     * @param cause        La nouvelle cause de la demande de congé.
     * @param dateDeb      La nouvelle date de début de la demande de congé.
     * @param dateFin      La nouvelle date de fin de la demande de congé.
     * @param isAccepted   Indique si la demande de congé est acceptée ou non.
     * @return La demande de congé mise à jour.
     */
    DemandeConge updateItem(String currentTitre, String titre, String cause, Date dateDeb, Date dateFin, boolean isAccepted);

    /**
     * Récupère toutes les demandes de congé.
     *
     * @return La liste de toutes les demandes de congé.
     */
    List<DemandeConge> getAll();

    /**
     * Récupère toutes les demandes de congé effectuées par un employé spécifique.
     *
     * @param userName Le nom d'utilisateur de l'employé.
     * @return La liste des demandes de congé effectuées par l'employé spécifié.
     */
    List<DemandeConge> getAllAskForVacationByEmployeeId(String userName);

    /**
     * Récupère une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé.
     * @return La demande de congé avec l'identifiant spécifié.
     */
    DemandeConge getById(long id);

    /**
     * Supprime une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé à supprimer.
     */
    void deleteById(long id);
}
