package com.myrh.service.impl;

import com.myrh.service.LoginAttemptService;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.concurrent.ExecutionException;

/**
 * Service de gestion des tentatives de connexion.
 */
@Service
public class LoginAttemptServiceImpl implements LoginAttemptService {

	/**
	 * Nombre maximum de tentatives autorisées.
	 */
	private static final int MAXIMUM_NUMBER_OF_ATTEMPTS = 5;

	/**
	 * Incrément des tentatives.
	 */
	private static final int ATTEMPTS_INCREMENT = 1;

	/**
	 * Cache des tentatives de connexion.
	 */
	private LoadingCache<String, Integer> loginAttemptCache;

	/**
	 * Constructeur par défaut. Initialise le cache des tentatives de connexion.
	 */
	public LoginAttemptServiceImpl() {
		super();
		loginAttemptCache = CacheBuilder.newBuilder()
				.expireAfterWrite(15, MINUTES) // le cache expire après 15 minutes
				.maximumSize(100)
				.build(new CacheLoader<String, Integer>() {
					public Integer load(String key) {
						return 0;
					}
				});
	}

	/**
	 * Supprime un utilisateur du cache des tentatives de connexion.
	 *
	 * @param username Le nom d'utilisateur à supprimer du cache.
	 */
	@Override
	public void removeUserFromLoginAttemptCache(String username) {
		loginAttemptCache.invalidate(username);
	}

	/**
	 * Ajoute un utilisateur au cache des tentatives de connexion.
	 *
	 * @param username Le nom d'utilisateur à ajouter au cache.
	 */
	@Override
	public void addUserToLoginAttemptCache(String username) {
		int attempts = 0;
		try {
			attempts = ATTEMPTS_INCREMENT + loginAttemptCache.get(username);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		loginAttemptCache.put(username, attempts);
	}

	/**
	 * Vérifie si un utilisateur a dépassé le nombre maximal de tentatives autorisées.
	 *
	 * @param username Le nom d'utilisateur à vérifier.
	 * @return Vrai s'il a dépassé le nombre maximal de tentatives, sinon faux.
	 */
	@Override
	public Boolean hasExceededMaxAttempts(String username) {
		try {
			return loginAttemptCache.get(username) >= MAXIMUM_NUMBER_OF_ATTEMPTS;
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
}
