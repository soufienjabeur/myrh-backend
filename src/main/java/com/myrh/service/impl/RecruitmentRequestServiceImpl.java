package com.myrh.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.myrh.domain.DemandeRecrutement;
import com.myrh.domain.Employee;
import com.myrh.domain.OpportuniteRecrutement;
import com.myrh.repository.RecruitmentRequestRepository;
import com.myrh.repository.EmployeeRepository;
import com.myrh.repository.RecruitmentOpportunityRepository;
import com.myrh.service.RecruitmentRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class RecruitmentRequestServiceImpl implements RecruitmentRequestService {

	@Autowired
	private RecruitmentRequestRepository recruitmentRequestRepository;

	@Autowired
	private RecruitmentOpportunityRepository recruitmentOpportunityRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * Récupère toutes les demandes de recrutement.
	 *
	 * @return Liste de toutes les demandes de recrutement.
	 */
	@Override
	public List<DemandeRecrutement> getAll() {
		return recruitmentRequestRepository.findAll();
	}

	/**
	 * Récupère une demande de recrutement par son ID.
	 *
	 * @param id L'ID de la demande de recrutement.
	 * @return La demande de recrutement avec l'ID spécifié.
	 */
	@Override
	public DemandeRecrutement getById(long id) {
		return recruitmentRequestRepository.findById(id).orElse(null);
	}

	/**
	 * Crée une nouvelle demande de recrutement.
	 *
	 * @param message           Le message accompagnant la demande.
	 * @param titreOpportunite  Le titre de l'opportunité de recrutement associée à la demande.
	 * @param cvFile            Le fichier CV joint à la demande.
	 * @param username          Le nom d'utilisateur de l'employé faisant la demande.
	 * @return La demande de recrutement créée.
	 */
	@Override
	public DemandeRecrutement saveItem1(String message, String titreOpportunite, MultipartFile cvFile, String username) {
		DemandeRecrutement demandeRecrutement = new DemandeRecrutement();
		demandeRecrutement.setMessage(message);

		// Associez l'Opportunite à la demande de recrutement
		OpportuniteRecrutement opportuniteRecrutement = recruitmentOpportunityRepository.findByTitre(titreOpportunite);
		demandeRecrutement.setOpportuniteRecrutement(opportuniteRecrutement);

		// Associez l'employé à la demande de recrutement
		Employee employee = employeeRepository.findEmployeeByUsername(username);
		demandeRecrutement.setEmployee(employee);

		// Vérifiez si un fichier CV a été fourni
		if (cvFile != null && !cvFile.isEmpty()) {
			try {
				// Générez un nom de fichier unique pour le CV
				String cvFileName = username + ".pdf";
				// Enregistrez le fichier CV dans un emplacement approprié (par exemple, dossier de téléchargement sur le serveur)
				String cvFilePath = saveCvFileToServer(cvFile, cvFileName);
				// Définissez le chemin du fichier CV dans la demande de recrutement
				demandeRecrutement.setCvFilePath(cvFilePath);
			} catch (Exception e) {
				// Gérez les erreurs lors de l'enregistrement du fichier CV
				e.printStackTrace();
			}
		}
		return recruitmentRequestRepository.save(demandeRecrutement);
	}

	/**
	 * Met à jour une demande de recrutement.
	 *
	 * @param currentMessage L'ancien message de la demande de recrutement.
	 * @param message        Le nouveau message pour la demande de recrutement.
	 * @param cvFile         Le nouveau fichier CV joint à la demande.
	 * @return La demande de recrutement mise à jour.
	 */
	@Override
	public DemandeRecrutement updateItem(String currentMessage, String message, MultipartFile cvFile) {
		DemandeRecrutement demandeRecrutement = recruitmentRequestRepository.findByMessage(currentMessage);
		demandeRecrutement.setMessage(message);

		if (cvFile != null && !cvFile.isEmpty()) {
			try {
				// Générer un nouveau nom de fichier unique pour le CV
				String cvFileName = demandeRecrutement.getEmployee().getUsername() + ".pdf";
				// Enregistrer le fichier CV dans un emplacement approprié
				String cvFilePath = saveCvFileToServer(cvFile, cvFileName);
				// Définir le nouveau chemin du fichier CV dans la demande de recrutement
				demandeRecrutement.setCvFilePath(cvFilePath);
			} catch (Exception e) {
				// Gérer les erreurs lors de l'enregistrement du fichier CV
				e.printStackTrace();
			}
		}

		return recruitmentRequestRepository.save(demandeRecrutement);
	}

	/**
	 * Supprime une demande de recrutement par son ID.
	 *
	 * @param id L'ID de la demande de recrutement à supprimer.
	 */
	@Override
	public void deeleteById(long id) {
		recruitmentRequestRepository.deleteById(id);
	}

	/**
	 * Enregistre le fichier CV sur le serveur.
	 *
	 * @param cvFile     Le fichier CV à enregistrer.
	 * @param cvFileName Le nom de fichier du CV.
	 * @return Le chemin du fichier CV enregistré sur le serveur.
	 * @throws IOException En cas d'erreur lors de l'enregistrement du fichier.
	 */
	private String saveCvFileToServer(MultipartFile cvFile, String cvFileName) throws IOException {
		String uploadDirectory = "C:\\Users\\pc\\OneDrive\\Bureau\\sami-akriche\\projet_acad_front\\src\\assets\\";
		// Créer le répertoire s'il n'existe pas
		File directory = new File(uploadDirectory);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		// Générer le chemin du fichier en concaténant le répertoire de téléchargement et le nom du fichier
		String cvFilePath = uploadDirectory + cvFileName;
		// Enregistrer le fichier CV à l'emplacement spécifié
		File outputFile = new File(cvFilePath);
		cvFile.transferTo(outputFile);

		return cvFilePath;
	}
}