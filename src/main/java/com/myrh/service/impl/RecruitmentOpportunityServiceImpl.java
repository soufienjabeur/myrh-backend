package com.myrh.service.impl;

import java.util.List;

import com.myrh.domain.DemandeRecrutement;
import com.myrh.domain.OpportuniteRecrutement;
import com.myrh.repository.RecruitmentOpportunityRepository;
import com.myrh.service.RecruitmentOpportunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service pour la gestion des opportunités de recrutement.
 */
@Service
public class RecruitmentOpportunityServiceImpl implements RecruitmentOpportunityService {

	@Autowired
	private RecruitmentOpportunityRepository recruitmentOpportunityRepository;

	/**
	 * Récupère toutes les opportunités de recrutement.
	 *
	 * @return La liste de toutes les opportunités de recrutement.
	 */
	@Override
	public List<OpportuniteRecrutement> getAll() {
		return recruitmentOpportunityRepository.findAll();
	}

	/**
	 * Récupère une opportunité de recrutement par son identifiant.
	 *
	 * @param id L'identifiant de l'opportunité de recrutement.
	 * @return L'opportunité de recrutement correspondant à l'identifiant.
	 */
	@Override
	public OpportuniteRecrutement getById(long id) {
		return recruitmentOpportunityRepository.findById(id).get();
	}

	/**
	 * Ajoute une nouvelle opportunité de recrutement.
	 *
	 * @param titre      Le titre de l'opportunité de recrutement.
	 * @param description La description de l'opportunité de recrutement.
	 * @return L'opportunité de recrutement ajoutée.
	 */
	@Override
	public OpportuniteRecrutement saveItem(String titre, String description) {
		OpportuniteRecrutement opRec = new OpportuniteRecrutement();
		opRec.setTitre(titre);
		opRec.setDescription(description);
		return recruitmentOpportunityRepository.save(opRec);
	}

	/**
	 * Met à jour une opportunité de recrutement existante.
	 *
	 * @param currentTitre L'ancien titre de l'opportunité de recrutement.
	 * @param titre         Le nouveau titre de l'opportunité de recrutement.
	 * @param description   La nouvelle description de l'opportunité de recrutement.
	 * @return L'opportunité de recrutement mise à jour.
	 */
	@Override
	public OpportuniteRecrutement updateItem(String currentTitre, String titre, String description) {
		OpportuniteRecrutement opRec = recruitmentOpportunityRepository.findByTitre(currentTitre);
		opRec.setTitre(titre);
		opRec.setDescription(description);
		return recruitmentOpportunityRepository.save(opRec);
	}

	/**
	 * Récupère la liste des demandes de recrutement associées à une opportunité de recrutement par son titre.
	 *
	 * @param titre Le titre de l'opportunité de recrutement.
	 * @return La liste des demandes de recrutement associées à l'opportunité de recrutement.
	 */
	@Override
	public List<DemandeRecrutement> getDemandeRecrutements(String titre) {
		OpportuniteRecrutement opportuniteRecrutement = recruitmentOpportunityRepository.findByTitre(titre);
		return opportuniteRecrutement.getDemandeRecrutements();
	}

	/**
	 * Supprime une opportunité de recrutement par son identifiant.
	 *
	 * @param id L'identifiant de l'opportunité de recrutement à supprimer.
	 */
	@Override
	public void deeleteById(long id) {
		recruitmentOpportunityRepository.deleteById(id);
	}
}
