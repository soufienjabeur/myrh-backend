package com.myrh.service.impl;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;
import com.myrh.service.EmailService;
import org.springframework.stereotype.Service;

import static com.myrh.constant.EmailConstant.*;

/**
 * Implémentation du service pour gérer les fonctionnalités liées aux e-mails dans l'application MyRH.
 * Ce service fournit des méthodes pour envoyer des e-mails de nouveaux mots de passe, des e-mails liés
 * aux congés, et des notifications sur les demandes de congé acceptées.
 */
@Service
public class EmailServiceImpl implements EmailService {

    /**
     * Envoie un e-mail de nouveau mot de passe à l'utilisateur.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param username  Le nom d'utilisateur de l'utilisateur.
     * @param password  Le nouveau mot de passe pour l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     */
    @Override
    public void sendNewPasswordEmail(String firstName, String username, String password, String email) {
        Message message;
        try {
            message = createMail(firstName, username, password, email);
            SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
            smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD);
            smtpTransport.sendMessage(message, message.getAllRecipients());
            smtpTransport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crée un message pour l'e-mail de nouveau mot de passe.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param username  Le nom d'utilisateur de l'utilisateur.
     * @param password  Le nouveau mot de passe pour l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     * @return Le message du courriel créé.
     * @throws MessagingException En cas d'erreur lors de la création du message.
     */
    private Message createMail(String firstName, String username, String password, String email) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC_EMAIL, false));
        message.setSubject(EMAIL_SUBJECT);
        message.setText("Bonjour " + firstName + "," + "\n\nVos identifiants pour vous connecter à votre espace sur notre application MyRH sont joints ci-dessous :\n\nNom d'utilisateur : " + username + "\nMot de passe : " + password + "\n\nCordialement");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    /**
     * Envoie un e-mail lié aux congés à l'utilisateur.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     */
    @Override
    public void sendEmailVacation(String firstName, String email) {
        Message message;
        try {
            message = createMailVacation(firstName, email);
            SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
            smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD);
            smtpTransport.sendMessage(message, message.getAllRecipients());
            smtpTransport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crée un message pour l'e-mail lié aux congés.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     * @return Le message du courriel créé.
     * @throws MessagingException En cas d'erreur lors de la création du message.
     */
    private Message createMailVacation(String firstName, String email) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC_EMAIL, false));
        message.setSubject(EMAIL_SUBJECT1);
        message.setText("Cher(e) " + firstName + ", \n Nous espérons que vous vous portez bien. Nous souhaitons vous informer que " +
                "votre demande de congé a été envoyée au responsable concerné pour examen." +
                "\n Nous comprenons à quel point il est important pour vous de prendre du temps pour vous reposer et " +
                "recharger vos batteries. Votre demande sera traitée dans les plus brefs délais et nous vous tiendrons informé(e) de la décision par e-mail." +
                "\n \n Cordialement");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    /**
     * Envoie une notification par e-mail à l'utilisateur concernant la demande de congé acceptée.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     * @param dateDebut La date de début des vacances approuvées.
     * @param dateFin   La date de fin des vacances approuvées.
     */
    @Override
    public void sendEmailVacationAccepted(String firstName, String email, Date dateDebut, Date dateFin) {
        Message message;
        try {
            message = mailVacationAccepted(firstName, email, dateDebut, dateFin);
            SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
            smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD);
            smtpTransport.sendMessage(message, message.getAllRecipients());
            smtpTransport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crée un message pour l'e-mail de notification de congé accepté.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param email     L'adresse e-mail de l'utilisateur.
     * @param dateDebut La date de début des vacances approuvées.
     * @param dateFin   La date de fin des vacances approuvées.
     * @return Le message du courriel créé.
     * @throws MessagingException En cas d'erreur lors de la création du message.
     */
    private Message mailVacationAccepted(String firstName, String email, Date dateDebut, Date dateFin) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC_EMAIL, false));
        message.setSubject(EMAIL_SUBJECT1);
        message.setText("Cher(e) " + firstName + ", \n Nous espérons que vous vous portez bien. Nous souhaitons vous informer que " +
                "votre demande de congé a été acceptée." +
                " \n Nous avons examiné attentivement votre demande et sommes ravis de vous accorder la période du " +
                dateDebut + " au " + dateFin + " en congé." +
                "\n Profitez pleinement de votre congé et utilisez ce temps pour vous détendre et vous ressourcer. Nous sommes convaincus que vous reviendrez avec une énergie renouvelée et une motivation accrue pour poursuivre votre excellent travail au sein de notre entreprise." +
                "\n \n Cordialement");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    /**
     * Récupère la session e-mail pour l'envoi d'e-mails.
     *
     * @return La session e-mail.
     */
    private Session getEmailSession() {
        Properties properties = System.getProperties();
        properties.put(SMTP_HOST, GMAIL_SMTP_SERVER);
        properties.put(SMTP_AUTH, true);
        properties.put(SMTP_PORT, DEFAULT_PORT);
        properties.put(SMTP_STARTTLS_ENABLE, true);
        properties.put(SMTP_STARTTLS_REQUIRED, true);
        return Session.getInstance(properties, null);
    }
}
