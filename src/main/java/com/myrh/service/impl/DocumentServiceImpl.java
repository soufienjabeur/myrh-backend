package com.myrh.service.impl;

import com.myrh.domain.Document;
import com.myrh.domain.Projet;
import com.myrh.repository.DocumentRepository;
import com.myrh.repository.ProjectRepository;
import com.myrh.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Service pour la gestion des documents.
 */
@Service
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	private ProjectRepository projectRepository;

	/**
	 * Enregistre un document avec le nom, le fichier et le titre du projet spécifiés.
	 *
	 * @param name         Le nom du document.
	 * @param documentUrl  Le fichier du document.
	 * @param titreProjet  Le titre du projet auquel le document est associé.
	 * @return Le document enregistré.
	 */
	@Override
	public Document saveItem(String name, MultipartFile documentUrl, String titreProjet) {
		Projet project = projectRepository.findByTitre(titreProjet);
		Document document = new Document();
		document.setProjet(project);
		project.getDocuments().add(document);
		document.setName(name);

		if (documentUrl != null && !documentUrl.isEmpty()) {
			try {

				String nameFile = document.getName() + ".pdf";
				String documentUrlPath = saveDocFileToServer(documentUrl, nameFile);
				document.setDocumentUrl(documentUrlPath);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return documentRepository.save(document);
	}

	/**
	 * Récupère tous les documents.
	 *
	 * @return Liste de tous les documents.
	 */
	@Override
	public List<Document> getAll() {
		return documentRepository.findAll();
	}

	/**
	 * Récupère un document par son identifiant.
	 *
	 * @param id L'identifiant du document.
	 * @return Le document correspondant à l'identifiant.
	 */
	@Override
	public Document getById(long id) {
		return documentRepository.findById(id).orElse(null);
	}

	/**
	 * Met à jour un document.
	 *
	 * @param document Le document à mettre à jour.
	 * @return Le document mis à jour.
	 */
	@Override
	public Document updateItem(Document document) {
		return documentRepository.save(document);
	}

	/**
	 * Supprime un document par son identifiant.
	 *
	 * @param id L'identifiant du document à supprimer.
	 */
	@Override
	public void deleteById(long id) {
		documentRepository.deleteById(id);
	}

	private String saveDocFileToServer(MultipartFile documentFile, String name) throws IOException {
		String uploadDirectory = "C:\\Users\\pc\\OneDrive\\Bureau\\sami-akriche\\projet_acad_front\\src\\assets\\";
		File directory = new File(uploadDirectory);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		String cvFilePath = uploadDirectory + name;
		File outputFile = new File(cvFilePath);
		documentFile.transferTo(outputFile);

		return cvFilePath;
	}
}