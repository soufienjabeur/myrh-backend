package com.myrh.service.impl;
import java.util.List;
import java.util.Optional;

import com.myrh.domain.Departement;
import com.myrh.domain.Employee;
import com.myrh.exception.domain.UserNotFoundException;
import com.myrh.repository.DepartmentRepository;
import com.myrh.repository.EmployeeRepository;
import com.myrh.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.myrh.constant.UserImplConstant.USER_NOT_FOUND;


/**
 * Service de gestion des départements.
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departementRepoitory;

	/**
	 * Récupère tous les départements.
	 *
	 * @return La liste de tous les départements.
	 */
	@Override
	public List<Departement> getAll() {
		return departementRepoitory.findAll();
	}

	/**
	 * Récupère un département par son identifiant.
	 *
	 * @param id L'identifiant du département.
	 * @return Le département correspondant à l'identifiant, ou {@code null} s'il n'est pas trouvé.
	 */
	@Override
	public Departement getById(long id) {
		return departementRepoitory.findById(id).orElse(null);
	}

	/**
	 * Enregistre un nouveau département avec le nom spécifié.
	 *
	 * @param name Le nom du nouveau département.
	 * @return Le département enregistré.
	 */
	@Override
	public Departement saveItem(String name) {
		Departement departement = new Departement();
		departement.setName(name);
		return departementRepoitory.save(departement);
	}

	/**
	 * Met à jour le nom d'un département.
	 *
	 * @param currentName Le nom actuel du département.
	 * @param name        Le nouveau nom du département.
	 * @return Le département mis à jour.
	 */
	@Override
	public Departement updateItem(String currentName, String name) {
		Departement departement = departementRepoitory.findDepartmentByName(currentName);
		departement.setName(name);
		return departementRepoitory.save(departement);
	}

	/**
	 * Récupère les membres d'un département par son identifiant.
	 *
	 * @param id L'identifiant du département.
	 * @return La liste des membres du département.
	 */
	@Override
	public List<Employee> getDepMembers(Long id) {
		Optional<Departement> departmentOptional = departementRepoitory.findById(id);
		Departement department = departmentOptional.orElseThrow(() -> new IllegalArgumentException("Department not found with id: " + id));
		return department.getEmployees();
	}

	/**
	 * Supprime un département par son identifiant.
	 *
	 * @param id L'identifiant du département à supprimer.
	 */
	@Override
	public void deleteById(long id) {
		Departement department = departementRepoitory.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Department not found with id: " + id));
		List<Employee> employees = department.getEmployees();
		for (Employee employee : employees) {
			employee.setDepartement(null);
		}
		employees.clear();
		departementRepoitory.deleteById(department.getId());
	}

	/**
	 * Affecte un employé à un département.
	 *
	 * @param username     Le nom d'utilisateur de l'employé.
	 * @param departmentId L'identifiant du département.
	 * @throws UserNotFoundException Si l'employé n'est pas trouvé.
	 */
	@Override
	public void assignEmployeeToDepartment(String username, Long departmentId) throws UserNotFoundException {
		Employee employee = employeeRepository.findEmployeeByUsername(username);
		Departement department = departementRepoitory.findById(departmentId)
				.orElseThrow(() -> new IllegalArgumentException("Department not found with ID: " + departmentId));

		if (employee == null) {
			throw new UserNotFoundException(USER_NOT_FOUND + username);
		} else {
			employee.setDepartement(department);
			department.getEmployees().add(employee);
			employeeRepository.save(employee);
		}
	}

	/**
	 * Retire un employé d'un département.
	 *
	 * @param username Le nom d'utilisateur de l'employé.
	 */
	@Override
	public void removeEmplFromDep(String username) {
		Employee employee = employeeRepository.findEmployeeByUsername(username);
		Departement departement = employee.getDepartement();
		if (departement != null) {
			departement.getEmployees().remove(employee);
			employee.setDepartement(null);
			employeeRepository.save(employee);
		}
	}
}