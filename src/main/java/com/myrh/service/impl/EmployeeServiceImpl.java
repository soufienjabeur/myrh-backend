package com.myrh.service.impl;

import com.myrh.domain.Employee;
import com.myrh.domain.UserPrincipal;
import com.myrh.enumeration.Role;
import com.myrh.exception.domain.EmailExistException;
import com.myrh.exception.domain.EmailNotFoundException;
import com.myrh.exception.domain.UsernameExistException;
import com.myrh.repository.EmployeeRepository;
import com.myrh.repository.RecruitmentRequestRepository;
import com.myrh.service.EmployeeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import static com.myrh.constant.FileConstant.*;
import static com.myrh.constant.UserImplConstant.DEFAULT_USER_IMAGE_PATH;
import static com.myrh.constant.UserImplConstant.*;
import static com.myrh.enumeration.Role.ROLE_USER;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Service implementation for Employee operations.
 */
@Service
@Transactional
@Qualifier("userDetailsService")
public class EmployeeServiceImpl implements EmployeeService, UserDetailsService {

    private EmployeeRepository userRepository;
    private RecruitmentRequestRepository recruitmentRequestRepository;
    private Logger LOGGER = LoggerFactory.getLogger(getClass());
    private BCryptPasswordEncoder passwordEncoder;
    private LoginAttemptServiceImpl loginAttemptServiceImpl;
    private EmailServiceImpl emailServiceImpl;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository userRepository, BCryptPasswordEncoder passwordEncoder, LoginAttemptServiceImpl loginAttemptServiceImpl, EmailServiceImpl emailServiceImpl, RecruitmentRequestRepository recruitmentRequestRepository) {
        super();
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.loginAttemptServiceImpl = loginAttemptServiceImpl;
        this.emailServiceImpl = emailServiceImpl;
        this.recruitmentRequestRepository = recruitmentRequestRepository;
    }

    /**
     * Récupère la liste des employés.
     *
     * @return Liste des employés.
     */
    @Override
    public List<Employee> getUsers() {
        return userRepository.findAll();
    }

    /**
     * Récupère un employé par son identifiant utilisateur.
     *
     * @param userId Identifiant de l'utilisateur.
     * @return Employé correspondant à l'identifiant.
     */
    @Override
    public Employee findUserByUserId(String userId) {
        return userRepository.findEmployeeByUserId(userId);
    }

    /**
     * Récupère un employé par son nom d'utilisateur.
     *
     * @param username Nom d'utilisateur de l'employé.
     * @return Employé correspondant au nom d'utilisateur.
     */
    @Override
    public Employee findUserByUserName(String username) {
        return userRepository.findEmployeeByUsername(username);
    }

    /**
     * Récupère un employé par son adresse e-mail.
     *
     * @param email Adresse e-mail de l'employé.
     * @return Employé correspondant à l'adresse e-mail.
     */
    @Override
    public Employee findUserByEmail(String email) {
        return userRepository.findEmployeeByEmail(email);
    }

    /**
     * Charge les détails de l'utilisateur par son nom d'utilisateur.
     *
     * @param username Nom d'utilisateur.
     * @return Détails de l'utilisateur.
     * @throws UsernameNotFoundException Si l'utilisateur n'est pas trouvé.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee user = userRepository.findEmployeeByUsername(username);
        if (user == null) {
            LOGGER.error(USER_NOT_FOUND + username);
            throw new UsernameNotFoundException(USER_NOT_FOUND + username);
        } else {
            validateLoginAttempts(user);
            user.setLastLoginDateDisplay(user.getLastLoginDate());
            user.setLastLoginDate(new Date());
            userRepository.save(user);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            LOGGER.info(FOUND_USER_BY_USERNAME + username);
            return userPrincipal;
        }
    }

    /**
     * Valide le nombre de tentatives de connexion pour un utilisateur.
     *
     * @param user Employé dont les tentatives de connexion sont validées.
     */
    private void validateLoginAttempts(Employee user) {
        if (user.isNotLocked()) {
            if (loginAttemptServiceImpl.hasExceededMaxAttempts(user.getUsername())) {
                user.setNotLocked(false);
            } else {
                user.setNotLocked(true);
            }
        } else {
            loginAttemptServiceImpl.removeUserFromLoginAttemptCache(user.getUsername());
        }
    }


    /**
     * Enregistre un nouvel employé.
     *
     * @param employee Employé à enregistrer.
     * @return Employé enregistré.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     */
    @Override
    public Employee register(Employee employee) throws UsernameExistException, EmailExistException {
        validateNewUsernameAndEmail(StringUtils.EMPTY, employee.getUsername(), employee.getEmail());
        Employee user = new Employee();
        user.setUserId(generateUserId());
        user.setFirstName(employee.getFirstName());
        user.setLastName(employee.getLastName());
        user.setUsername(employee.getUsername());
        user.setEmail(employee.getEmail());
        user.setJoinDate(new Date());
        user.setActive(true);
        user.setNotLocked(true);
        user.setRole(ROLE_USER.name());  // ROLE visiteur
        user.setProfileImageUrl(getTemporaryProfileImageUrl(employee.getUsername()));
        user.setAuthorities(ROLE_USER.getAuthorities());// ROLE visiteur
        user.setPhoneNumber(employee.getPhoneNumber());
        user.setAddress(employee.getAddress());
        user.setGender(employee.getGender());
        user.setMaritalStatus(employee.getMaritalStatus());
        String password = generatePassword();
        user.setPassword(encodePassword(password));
        userRepository.save(user);
        LOGGER.info("Création d'un nouvel utilisateur : {}", user.getUsername());
        emailServiceImpl.sendNewPasswordEmail(employee.getFirstName(), employee.getUsername(), password, employee.getEmail());
        return user;
    }


    /**
     * Ajoute un nouvel utilisateur.
     *
     * @param firstName    Prénom de l'utilisateur.
     * @param lastName     Nom de l'utilisateur.
     * @param username     Nom d'utilisateur.
     * @param email        Adresse e-mail.
     * @param role         Rôle de l'utilisateur.
     * @param isNotLocked  Statut de verrouillage de l'utilisateur.
     * @param isActive     Statut actif de l'utilisateur.
     * @param profileImage Image de profil de l'utilisateur.
     * @return L'utilisateur ajouté.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    @Override
    public Employee addNewUser(String firstName, String lastName, String username, String email, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException {
        validateNewUsernameAndEmail(null, username, email);
        Employee user = new Employee();
        String password = generatePassword();
        user.setPassword(encodePassword(password));
        user.setUserId(generateUserId());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setEmail(email);
        user.setJoinDate(new Date());
        user.setActive(true);
        user.setNotLocked(true);
        user.setPaidVacations(30);
        user.setRole(getRoleEnumName(role).name());
        user.setAuthorities(getRoleEnumName(role).getAuthorities());
        user.setProfileImageUrl(getTemporaryProfileImageUrl(username));
        saveProfileImage(user, profileImage);
        userRepository.save(user);
        emailServiceImpl.sendNewPasswordEmail(firstName, username, password, email);
        LOGGER.info("Ajout d'un nouvel utilisateur : {}", user.getUsername());
        return user;
    }


    /**
     * Met à jour un utilisateur.
     *
     * @param currentUsername Ancien nom d'utilisateur.
     * @param newFirstName    Nouveau prénom.
     * @param newLastName     Nouveau nom.
     * @param newUsername     Nouveau nom d'utilisateur.
     * @param newEmail        Nouvelle adresse e-mail.
     * @param role            Nouveau rôle.
     * @param isNotLocked     Nouveau statut de verrouillage.
     * @param isActive        Nouveau statut actif.
     * @param profileImage    Nouvelle image de profil.
     * @return L'utilisateur mis à jour.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    @Override
    public Employee updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException {
        Employee currentUser = validateNewUsernameAndEmail(currentUsername, newUsername, newEmail);
        currentUser.setFirstName(newFirstName);
        currentUser.setLastName(newLastName);
        currentUser.setUsername(newUsername);
        currentUser.setEmail(newEmail);
        currentUser.setJoinDate(new Date());
        currentUser.setActive(isActive);
        currentUser.setNotLocked(isNotLocked);
        currentUser.setRole(getRoleEnumName(role).name());
        currentUser.setAuthorities(getRoleEnumName(role).getAuthorities());
        userRepository.save(currentUser);
        saveProfileImage(currentUser, profileImage);
        LOGGER.info("Mise à jour de l'utilisateur : {}", currentUser.getUsername());
        return currentUser;
    }


    /**
     * Supprime un utilisateur.
     *
     * @param username Nom d'utilisateur de l'utilisateur à supprimer.
     * @throws IOException En cas d'erreur d'entrée/sortie.
     */
    @Override
    public void deleteUser(String username) throws IOException {
        Employee employee = userRepository.findEmployeeByUsername(username);
        recruitmentRequestRepository.deleteByEmployeeId(employee.getId());

        Path userFolder = Paths.get(USER_FOLDER + employee.getUsername()).toAbsolutePath().normalize();
        FileUtils.deleteDirectory(new File(userFolder.toString()));
        userRepository.deleteById(employee.getId());
        LOGGER.info("Suppression de l'utilisateur : {}", username);
    }

    /**
     * Réinitialise le mot de passe d'un utilisateur.
     *
     * @param email Adresse e-mail de l'utilisateur.
     * @throws EmailNotFoundException Si l'adresse e-mail n'est pas trouvée.
     */
    @Override
    public void resetPassword(String email) throws EmailNotFoundException {
        Employee user = findUserByEmail(email);
        if (user == null) {
            throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);
        }
        String password = generatePassword();
        user.setPassword(encodePassword(password));
        userRepository.save(user);
        LOGGER.info("Nouveau mot de passe pour l'utilisateur {} : {}", user.getUsername(), password);
        emailServiceImpl.sendNewPasswordEmail(user.getFirstName(), user.getUsername(), password, email);
    }

    /**
     * Met à jour l'image de profil d'un utilisateur.
     *
     * @param username     Nom d'utilisateur de l'utilisateur.
     * @param profileImage Nouvelle image de profil.
     * @return L'utilisateur avec l'image de profil mise à jour.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    @Override
    public Employee updateProfileImage(String username, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException {
        Employee user = validateNewUsernameAndEmail(username, null, null);
        saveProfileImage(user, profileImage);
        LOGGER.info("Mise à jour de l'image de profil de l'utilisateur : {}", user.getUsername());
        return user;
    }


    /**
     * Sauvegarde l'image de profil de l'utilisateur dans le système de fichiers.
     *
     * @param user         Employé dont l'image de profil est sauvegardée.
     * @param profileImage Nouvelle image de profil.
     * @throws IOException En cas d'erreur d'entrée/sortie.
     */
    private void saveProfileImage(Employee user, MultipartFile profileImage) throws IOException {
        if (profileImage != null) {
            Path userFolder = Paths.get(USER_FOLDER + user.getUsername()).toAbsolutePath().normalize();
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
                LOGGER.info(DIRECTORY_CREATED + userFolder);
            }
            Files.deleteIfExists(Paths.get(USER_FOLDER + user.getUsername() + DOT + JPG_EXTENSION));
            Files.copy(profileImage.getInputStream(), userFolder.resolve(user.getUsername() + DOT + JPG_EXTENSION), REPLACE_EXISTING);
            user.setProfileImageUrl(setProfileImage(user.getUsername()));
            userRepository.save(user);
            LOGGER.info(FILE_SAVED_IN_FILE_SYSTEM + profileImage.getOriginalFilename());
        }
    }

    /**
     * Définit l'URL de l'image de profil pour un employé.
     *
     * @param username Nom d'utilisateur de l'employé.
     * @return L'URL de l'image de profil.
     */
    private String setProfileImage(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(USER_IMAGE_PATH + username + FORWARD_SLASH + username + DOT + JPG_EXTENSION).toUriString();
    }

    /**
     * Convertit une chaîne de rôle en une énumération Role.
     *
     * @param role La chaîne de rôle.
     * @return L'énumération Role correspondante.
     */
    private Role getRoleEnumName(String role) {
        return Role.valueOf(role.toUpperCase());
    }

    /**
     * Génère un identifiant utilisateur aléatoire.
     *
     * @return Identifiant utilisateur généré.
     */
    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }

    /**
     * Génère un mot de passe aléatoire.
     *
     * @return Mot de passe généré.
     */
    private String generatePassword() {
        return RandomStringUtils.randomAlphabetic(10);
    }

    /**
     * Encode un mot de passe avec l'algorithme BCrypt.
     *
     * @param password Mot de passe à encoder.
     * @return Mot de passe encodé.
     */
    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     * Obtient l'URL temporaire de l'image de profil par défaut.
     *
     * @param username Nom d'utilisateur.
     * @return L'URL temporaire de l'image de profil par défaut.
     */
    private String getTemporaryProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_USER_IMAGE_PATH + username).toUriString();
    }

    /**
     * Valide le nouveau nom d'utilisateur et l'adresse e-mail.
     *
     * @param currentUsername Ancien nom d'utilisateur.
     * @param newUsername     Nouveau nom d'utilisateur.
     * @param newEmail        Nouvelle adresse e-mail.
     * @return L'employé après la validation.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     */
    private Employee validateNewUsernameAndEmail(String currentUsername, String newUsername, String newEmail) throws UsernameExistException, EmailExistException {
        Employee userByUsername = findUserByUserName(newUsername);
        Employee userByEmail = findUserByEmail(newEmail);
        Employee userByNewEmail = findUserByEmail(newEmail);
        if (StringUtils.isNotBlank(currentUsername)) {
            Employee currentUser = findUserByUserName(currentUsername);
            Employee userByNewUsername = findUserByUserName(newUsername);
            if (currentUser == null) {
                throw new UsernameNotFoundException(USER_NOT_FOUND + currentUser);
            }
            if (userByNewUsername != null && !currentUser.getId().equals(userByNewUsername.getId())) {
                throw new UsernameExistException(USERNAME_ALREADY_EXISTS);
            }
            if (userByNewEmail != null && !currentUser.getId().equals(userByNewEmail.getId())) {
                throw new EmailExistException(EMAIL_ALREADY_EXISTS);
            }
            return currentUser;
        } else {
            if (userByUsername != null) {
                throw new UsernameExistException(USERNAME_ALREADY_EXISTS);
            }
            if (userByEmail != null) {
                throw new EmailExistException(EMAIL_ALREADY_EXISTS);
            }
            return null;
        }
    }
}
