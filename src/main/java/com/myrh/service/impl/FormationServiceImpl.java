package com.myrh.service.impl;

import com.myrh.domain.Formation;
import com.myrh.repository.FormationRepository;
import com.myrh.service.FormationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Service pour la gestion des formations.
 */
@Service
public class FormationServiceImpl implements FormationService {

	@Autowired
	private FormationRepository formationRepository;

	/**
	 * Ajoute une nouvelle formation.
	 *
	 * @param titre   Le titre de la formation.
	 * @param dateDeb La date de début de la formation.
	 * @param dateFin La date de fin de la formation.
	 * @return La formation ajoutée.
	 */
	@Override
	public Formation saveItem(String titre, Date dateDeb, Date dateFin) {
		Formation formation = new Formation();
		formation.setTitre(titre);
		formation.setDateDebut(dateDeb);
		formation.setDateFin(dateFin);
		return formationRepository.save(formation);
	}

	/**
	 * Met à jour une formation existante.
	 *
	 * @param currentTitre Le titre actuel de la formation.
	 * @param titre        Le nouveau titre de la formation.
	 * @param dateDeb      La nouvelle date de début de la formation.
	 * @param dateFin      La nouvelle date de fin de la formation.
	 * @return La formation mise à jour.
	 */
	@Override
	public Formation updateItem(String currentTitre, String titre, Date dateDeb, Date dateFin) {
		Formation formation = formationRepository.findByTitre(currentTitre);
		formation.setTitre(titre);
		formation.setDateDebut(dateDeb);
		formation.setDateFin(dateFin);
		return formationRepository.save(formation);
	}

	/**
	 * Récupère toutes les formations.
	 *
	 * @return La liste de toutes les formations.
	 */
	@Override
	public List<Formation> getAll() {
		return formationRepository.findAll();
	}

	/**
	 * Récupère une formation par son identifiant.
	 *
	 * @param id L'identifiant de la formation.
	 * @return La formation correspondant à l'identifiant.
	 */
	@Override
	public Formation getById(long id) {
		return formationRepository.findById(id).orElse(null);
	}

	/**
	 * Supprime une formation par son identifiant.
	 *
	 * @param id L'identifiant de la formation à supprimer.
	 */
	@Override
	public void deleteById(long id) {
		formationRepository.deleteById(id);
	}
}
