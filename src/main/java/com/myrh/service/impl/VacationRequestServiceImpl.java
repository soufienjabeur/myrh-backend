package com.myrh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;


import com.myrh.domain.DemandeConge;
import com.myrh.domain.Employee;
import com.myrh.repository.VacationRequestRepository;
import com.myrh.repository.EmployeeRepository;
import com.myrh.service.EmailService;
import com.myrh.service.VacationRequestService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Implémentation du service de gestion des demandes de congé.
 */
@Service
@AllArgsConstructor
public class VacationRequestServiceImpl implements VacationRequestService {

    @Autowired
    private final VacationRequestRepository vacationRequestRepository;
    @Autowired
    private final EmployeeRepository employeeRepository;
    @Autowired
    private final EmailService emailService;

    /**
     * Constructeur pour l'injection des dépendances.
     *
     * @param emailService               Le service d'e-mails.
     * @param vacationRequestRepository  Le repository des demandes de congé.
     * @param employeeRepository         Le repository des employés.
     */
    @Autowired
    public VacationRequestServiceImpl(EmailService emailService, VacationRequestRepository vacationRequestRepository, EmployeeRepository employeeRepository) {
        super();
        this.employeeRepository = employeeRepository;
        this.vacationRequestRepository = vacationRequestRepository;
        this.emailService = emailService;
    }

    /**
     * Enregistre une nouvelle demande de congé.
     *
     * @param titre     Le titre de la demande.
     * @param cause     La raison de la demande.
     * @param dateDeb   La date de début de la demande.
     * @param dateFin   La date de fin de la demande.
     * @param username  Le nom d'utilisateur de l'employé effectuant la demande.
     * @return La demande de congé enregistrée.
     */
    @Override
    public DemandeConge saveItem(String titre, String cause, Date dateDeb, Date dateFin, String username) {

        Employee employee = employeeRepository.findEmployeeByUsername(username);
        DemandeConge demandeConge = new DemandeConge();
        demandeConge.setTitre(titre);
        demandeConge.setCause(cause);
        demandeConge.setDateDebut(dateDeb);
        demandeConge.setDateFin(dateFin);
        demandeConge.setIsAccepted(false);
        demandeConge.setEmployee(employee);

        String email = employee.getEmail();
        String firstName = employee.getFirstName();
        emailService.sendEmailVacation(firstName, email);
        return vacationRequestRepository.save(demandeConge);
    }

    /**
     * Met à jour une demande de congé existante.
     *
     * @param currenTitre  Le titre actuel de la demande.
     * @param titre        Le nouveau titre de la demande.
     * @param cause        La nouvelle raison de la demande.
     * @param dateDeb      La nouvelle date de début de la demande.
     * @param dateFin      La nouvelle date de fin de la demande.
     * @param isAccepted   Indique si la demande est acceptée ou non.
     * @return La demande de congé mise à jour.
     */
    @Override
    public DemandeConge updateItem(String currenTitre, String titre, String cause, Date dateDeb, Date dateFin, boolean isAccepted) {

        DemandeConge demandeConge = vacationRequestRepository.findByTitre(currenTitre);
        Employee employee = demandeConge.getEmployee();

        String email = employee.getEmail();
        String firstName = employee.getFirstName();
        String newTitre = this.isEmptyOrBlankString(titre) ? currenTitre : titre;
        String newCause = this.isEmptyOrBlankString(cause) ? demandeConge.getCause() : cause;
        Date newDateDeb = Objects.isNull(dateDeb) || "NaN-NaN-NaN".equals(dateDeb.toString()) ? demandeConge.getDateDebut() : dateDeb;
        Date newDateFin = Objects.isNull(dateFin) || "NaN-NaN-NaN".equals(dateFin.toString()) ? demandeConge.getDateFin() : dateFin;
        demandeConge.setTitre(newTitre);
        demandeConge.setDateDebut(newDateDeb);
        demandeConge.setCause(newCause);
        demandeConge.setDateFin(newDateFin);
        demandeConge.setIsAccepted(isAccepted);

        if (isAccepted) {
            emailService.sendEmailVacationAccepted(firstName, email, dateDeb, dateFin);
        }
        return vacationRequestRepository.save(demandeConge);
    }

    /**
     * Récupère toutes les demandes de congé.
     *
     * @return La liste de toutes les demandes de congé.
     */
    @Override
    public List<DemandeConge> getAll() {
        return vacationRequestRepository.findAll();
    }

    /**
     * Récupère toutes les demandes de congé effectuées par un employé.
     *
     * @param userName Le nom d'utilisateur de l'employé.
     * @return La liste des demandes de congé effectuées par l'employé.
     */
    @Override
    public List<DemandeConge> getAllAskForVacationByEmployeeId(String userName) {
        Employee employee = this.employeeRepository.findEmployeeByUsername(userName);
        return vacationRequestRepository.findByEmployeeId(employee.getId());
    }

    /**
     * Récupère une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé.
     * @return La demande de congé correspondante.
     */
    @Override
    public DemandeConge getById(long id) {
        return vacationRequestRepository.findById(id).orElse(null);
    }

    /**
     * Supprime une demande de congé par son identifiant.
     *
     * @param id L'identifiant de la demande de congé à supprimer.
     */
    @Override
    public void deleteById(long id) {
        vacationRequestRepository.deleteById(id);
    }

    /**
     * Vérifie si une chaîne de caractères est nulle, vide ou composée uniquement d'espaces.
     *
     * @param str La chaîne de caractères à vérifier.
     * @return true si la chaîne est nulle, vide ou composée uniquement d'espaces, sinon false.
     */
    private boolean isEmptyOrBlankString(String str) {
        return Objects.isNull(str) || str.isEmpty() || str.isBlank();
    }
}
