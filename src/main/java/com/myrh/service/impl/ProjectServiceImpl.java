package com.myrh.service.impl;


import com.myrh.domain.Document;
import com.myrh.domain.Projet;
import com.myrh.repository.ProjectRepository;
import com.myrh.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



/**
 * Service de gestion des projets.
 */
@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	/**
	 * Récupère tous les projets.
	 *
	 * @return La liste de tous les projets.
	 */
	@Override
	public List<Projet> getAll() {
		return projectRepository.findAll();
	}

	/**
	 * Récupère un projet par son identifiant.
	 *
	 * @param id L'identifiant du projet.
	 * @return Le projet correspondant à l'identifiant.
	 */
	@Override
	public Projet getById(long id) {
		return projectRepository.findById(id).orElse(null);
	}

	/**
	 * Ajoute un nouveau projet.
	 *
	 * @param titre   Le titre du projet.
	 * @param descrip La description du projet.
	 * @return Le projet ajouté.
	 */
	@Override
	public Projet saveItem(String titre, String descrip) {
		Projet projet = new Projet();
		projet.setTitre(titre);
		projet.setDescription(descrip);
		return projectRepository.save(projet);
	}

	/**
	 * Met à jour un projet existant.
	 *
	 * @param currentTitre L'ancien titre du projet.
	 * @param titre        Le nouveau titre du projet.
	 * @param descrip      La nouvelle description du projet.
	 * @return Le projet mis à jour.
	 */
	@Override
	public Projet updateItem(String currentTitre, String titre, String descrip) {
		Projet projet = projectRepository.findByTitre(currentTitre);
		projet.setTitre(titre);
		projet.setDescription(descrip);
		return projectRepository.save(projet);
	}

	/**
	 * Récupère la liste des documents associés à un projet par son titre.
	 *
	 * @param titreProjet Le titre du projet.
	 * @return La liste des documents associés au projet.
	 * @throws IllegalArgumentException Si le projet n'est pas trouvé avec le titre spécifié.
	 */
	@Override
	public List<Document> getDocumentsByTitreProjet(String titreProjet) {
		Projet projet = projectRepository.findByTitre(titreProjet);
		if (projet == null) {
			throw new IllegalArgumentException("Projet not found with titre: " + titreProjet);
		}
		return projet.getDocuments();
	}

	/**
	 * Supprime un projet par son identifiant.
	 *
	 * @param id L'identifiant du projet à supprimer.
	 */
	@Override
	public void deeleteById(long id) {
		projectRepository.deleteById(id);
	}

}
