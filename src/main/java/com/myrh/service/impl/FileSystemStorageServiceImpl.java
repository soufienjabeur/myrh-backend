package com.myrh.service.impl;


import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.annotation.PostConstruct;

import com.myrh.exception.domain.FileNotFoundException;
import com.myrh.service.FileSystemStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


/**
 * Implémentation du service de stockage de fichiers dans le système de fichiers.
 */
@Service
public class FileSystemStorageServiceImpl implements FileSystemStorageService {

    private final Path dirLocation;

    /**
     * Initialise le répertoire de stockage des fichiers.
     */
    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(this.dirLocation);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Constructeur de la classe FileSystemStorageServiceImpl.
     * Définit le répertoire de stockage par défaut.
     */
    public FileSystemStorageServiceImpl() {
        this.dirLocation = Paths.get("/upload").toAbsolutePath().normalize();
    }

    /**
     * Enregistre un fichier dans le système de fichiers.
     *
     * @param file Le fichier à enregistrer.
     * @return Le nom du fichier enregistré.
     */
    @Override
    public String saveFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            Path dfile = this.dirLocation.resolve(fileName);
            Files.copy(file.getInputStream(), dfile, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (Exception e) {
            return "Impossible d'uploader le fichier";
        }
    }

    /**
     * Charge un fichier à partir du système de fichiers.
     *
     * @param fileName Le nom du fichier à charger.
     * @return La ressource représentant le fichier chargé.
     * @throws FileNotFoundException Si le fichier n'est pas trouvé.
     */
    @Override
    public Resource loadFile(String fileName) throws FileNotFoundException {
        try {
            Path file = this.dirLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new FileNotFoundException("Impossible de trouver le fichier");
            }
        } catch (MalformedURLException e) {
            throw new FileNotFoundException("Impossible de télécharger le fichier");
        }
    }
}