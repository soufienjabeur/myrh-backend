package com.myrh.service;

import java.util.Date;

/**
 * Interface pour le service d'envoi d'e-mails.
 * Fournit des méthodes pour envoyer différents types d'e-mails.
 */
public interface EmailService {

    /**
     * Envoie un e-mail avec les informations du nouveau mot de passe.
     *
     * @param firstName Le prénom de l'utilisateur.
     * @param username  Le nom d'utilisateur de l'utilisateur.
     * @param password  Le nouveau mot de passe.
     * @param email     L'adresse e-mail de l'utilisateur.
     */
    void sendNewPasswordEmail(String firstName, String username, String password, String email);

    /**
     * Envoie un e-mail pour informer de l'envoi d'une demande de congé.
     *
     * @param firstName Le prénom de l'employé.
     * @param email     L'adresse e-mail de l'employé.
     */
    void sendEmailVacation(String firstName, String email);

    /**
     * Envoie un e-mail pour informer de l'acceptation d'une demande de congé.
     *
     * @param firstName Le prénom de l'employé.
     * @param email     L'adresse e-mail de l'employé.
     * @param dateDebut La date de début de la demande de congé acceptée.
     * @param dateFin   La date de fin de la demande de congé acceptée.
     */
    void sendEmailVacationAccepted(String firstName, String email, Date dateDebut, Date dateFin);
}
