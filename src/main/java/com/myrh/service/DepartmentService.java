package com.myrh.service;

import com.myrh.domain.Departement;
import com.myrh.domain.Employee;
import com.myrh.exception.domain.UserNotFoundException;

import java.util.List;

/**
 * Interface pour le service de gestion des départements.
 */
public interface DepartmentService {

    /**
     * Récupère tous les départements.
     *
     * @return La liste de tous les départements.
     */
    List<Departement> getAll();

    /**
     * Récupère un département par son identifiant.
     *
     * @param id L'identifiant du département.
     * @return Le département correspondant à l'identifiant, ou {@code null} s'il n'est pas trouvé.
     */
    Departement getById(long id);

    /**
     * Enregistre un nouveau département avec le nom spécifié.
     *
     * @param name Le nom du nouveau département.
     * @return Le département enregistré.
     */
    Departement saveItem(String name);

    /**
     * Met à jour le nom d'un département.
     *
     * @param currentName Le nom actuel du département.
     * @param name        Le nouveau nom du département.
     * @return Le département mis à jour.
     */
    Departement updateItem(String currentName, String name);

    /**
     * Récupère les membres d'un département par son identifiant.
     *
     * @param id L'identifiant du département.
     * @return La liste des membres du département.
     */
    List<Employee> getDepMembers(Long id);

    /**
     * Supprime un département par son identifiant.
     *
     * @param id L'identifiant du département à supprimer.
     */
    void deleteById(long id);

    /**
     * Affecte un employé à un département.
     *
     * @param username     Le nom d'utilisateur de l'employé.
     * @param departmentId L'identifiant du département.
     * @throws UserNotFoundException Si l'employé n'est pas trouvé.
     */
    void assignEmployeeToDepartment(String username, Long departmentId) throws UserNotFoundException;

    /**
     * Retire un employé d'un département.
     *
     * @param username Le nom d'utilisateur de l'employé.
     */
    void removeEmplFromDep(String username);
}

