package com.myrh.service;

import com.myrh.domain.Employee;
import com.myrh.exception.domain.EmailExistException;
import com.myrh.exception.domain.EmailNotFoundException;
import com.myrh.exception.domain.UserNotFoundException;
import com.myrh.exception.domain.UsernameExistException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Cette interface définit les opérations de service pour la gestion des employés.
 */
public interface EmployeeService {

    /**
     * Enregistre un nouvel employé dans le système.
     *
     * @param employe L'employé à enregistrer.
     * @return L'employé enregistré.
     * @throws UserNotFoundException  Si l'utilisateur n'est pas trouvé.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     */
    Employee register(Employee employe) throws UserNotFoundException, UsernameExistException, EmailExistException;

    /**
     * Récupère la liste de tous les employés du système.
     *
     * @return La liste des employés.
     */
    List<Employee> getUsers();

    /**
     * Recherche un employé par son identifiant utilisateur.
     *
     * @param userId L'identifiant utilisateur de l'employé.
     * @return L'employé correspondant à l'identifiant utilisateur.
     */
    Employee findUserByUserId(String userId);

    /**
     * Recherche un employé par son nom d'utilisateur.
     *
     * @param username Le nom d'utilisateur de l'employé.
     * @return L'employé correspondant au nom d'utilisateur.
     */
    Employee findUserByUserName(String username);

    /**
     * Recherche un employé par son adresse e-mail.
     *
     * @param email L'adresse e-mail de l'employé.
     * @return L'employé correspondant à l'adresse e-mail.
     */
    Employee findUserByEmail(String email);

    /**
     * Ajoute un nouvel employé au système.
     *
     * @param firstName    Le prénom de l'employé.
     * @param lastName     Le nom de famille de l'employé.
     * @param username     Le nom d'utilisateur de l'employé.
     * @param email        L'adresse e-mail de l'employé.
     * @param role         Le rôle de l'employé.
     * @param isNotLoked   Indique si l'employé est verrouillé ou non.
     * @param isActive     Indique si l'employé est actif ou non.
     * @param profileImage L'image de profil de l'employé.
     * @return L'employé ajouté.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    Employee addNewUser(String firstName, String lastName, String username, String email, String role, boolean isNotLoked, boolean isActive, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException;

    /**
     * Met à jour les informations d'un employé existant.
     *
     * @param currentUsername Le nom d'utilisateur actuel de l'employé.
     * @param newFirstName    Le nouveau prénom de l'employé.
     * @param newLastName     Le nouveau nom de famille de l'employé.
     * @param newUsername     Le nouveau nom d'utilisateur de l'employé.
     * @param newEmail        La nouvelle adresse e-mail de l'employé.
     * @param role            Le nouveau rôle de l'employé.
     * @param isNotLoked      Indique si l'employé est verrouillé ou non.
     * @param isActive        Indique si l'employé est actif ou non.
     * @param profileImage    La nouvelle image de profil de l'employé.
     * @return L'employé mis à jour.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    Employee updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail, String role, boolean isNotLoked, boolean isActive, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException;

    /**
     * Supprime un employé du système.
     *
     * @param username Le nom d'utilisateur de l'employé à supprimer.
     * @throws IOException En cas d'erreur d'entrée/sortie.
     */
    void deleteUser(String username) throws IOException;

    /**
     * Réinitialise le mot de passe d'un employé par e-mail.
     *
     * @param email L'adresse e-mail de l'employé.
     * @throws EmailNotFoundException Si l'adresse e-mail n'est pas trouvée.
     */
    void resetPassword(String email) throws EmailNotFoundException;

    /**
     * Met à jour l'image de profil d'un employé.
     *
     * @param username     Le nom d'utilisateur de l'employé.
     * @param profileImage La nouvelle image de profil de l'employé.
     * @return L'employé avec l'image de profil mise à jour.
     * @throws UsernameExistException Si le nom d'utilisateur existe déjà.
     * @throws EmailExistException    Si l'adresse e-mail existe déjà.
     * @throws IOException            En cas d'erreur d'entrée/sortie.
     */
    Employee updateProfileImage(String username, MultipartFile profileImage) throws UsernameExistException, EmailExistException, IOException;
}
