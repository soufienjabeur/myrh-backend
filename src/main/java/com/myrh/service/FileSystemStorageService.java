package com.myrh.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Interface pour le stockage des fichiers dans le système de fichiers.
 */
public interface FileSystemStorageService {

    /**
     * Initialise le système de stockage des fichiers.
     */
    void init();

    /**
     * Enregistre un fichier dans le système de fichiers.
     *
     * @param file Le fichier à enregistrer.
     * @return Le nom du fichier enregistré.
     */
    String saveFile(MultipartFile file);

    /**
     * Charge un fichier à partir du système de fichiers.
     *
     * @param fileName Le nom du fichier à charger.
     * @return La ressource représentant le fichier chargé.
     */
    Resource loadFile(String fileName);
}
