package com.myrh.repository;

import com.myrh.domain.DemandeConge;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface de repository pour la gestion des entités VacationRequest.
 * Fournit des méthodes pour interagir avec la table de base de données VacationRequest.
 */
public interface VacationRequestRepository extends JpaRepository<DemandeConge, Long> {

    /**
     * Recherche une demande de congé par son titre.
     *
     * @param title Le titre de la demande de congé.
     * @return La demande de congé avec le titre spécifié, ou null si elle n'est pas trouvée.
     */
    DemandeConge findByTitre(String title);

    /**
     * Recherche les demandes de congé associées à l'identifiant d'un employé.
     *
     * @param employeeId L'identifiant de l'employé pour lequel on recherche les demandes de congé.
     * @return La liste des demandes de congé associées à l'employé, ou une liste vide si aucune n'est trouvée.
     */
    List<DemandeConge> findByEmployeeId(Long employeeId);
}

