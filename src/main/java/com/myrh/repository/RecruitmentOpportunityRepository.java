package com.myrh.repository;
import com.myrh.domain.OpportuniteRecrutement;
import org.springframework.data.jpa.repository.JpaRepository;



/**
 * Interface de repository pour la gestion des opportunités de recrutement.
 * Fournit des méthodes pour interagir avec la table de base de données OpportuniteRecrutement.
 */
public interface RecruitmentOpportunityRepository extends JpaRepository<OpportuniteRecrutement, Long> {

    /**
     * Recherche une opportunité de recrutement par son titre.
     *
     * @param currentTitre Le titre de l'opportunité de recrutement.
     * @return L'opportunité de recrutement avec le titre spécifié, ou null si elle n'est pas trouvée.
     */
    OpportuniteRecrutement findByTitre(String currentTitre);
}
