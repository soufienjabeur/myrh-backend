package com.myrh.repository;

import com.myrh.domain.Document;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface de repository pour la gestion des entités Document.
 * Fournit des méthodes pour interagir avec la table de base de données Document.
 */
public interface DocumentRepository extends JpaRepository<Document, Long> {

}

