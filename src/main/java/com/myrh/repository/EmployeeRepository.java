package com.myrh.repository;

import com.myrh.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface de repository pour la gestion des entités Employee.
 * Fournit des méthodes pour interagir avec la table de base de données Employee.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    /**
     * Récupère un employé par son nom d'utilisateur.
     *
     * @param username Le nom d'utilisateur de l'employé.
     * @return L'employé avec le nom d'utilisateur spécifié, ou null s'il n'est pas trouvé.
     */
    Employee findEmployeeByUsername(String username);

    /**
     * Récupère un employé par son identifiant d'utilisateur.
     *
     * @param userId L'identifiant d'utilisateur de l'employé.
     * @return L'employé avec l'identifiant d'utilisateur spécifié, ou null s'il n'est pas trouvé.
     */
    Employee findEmployeeByUserId(String userId);

    /**
     * Récupère un employé par son adresse e-mail.
     *
     * @param email L'adresse e-mail de l'employé.
     * @return L'employé avec l'adresse e-mail spécifiée, ou null s'il n'est pas trouvé.
     */
    Employee findEmployeeByEmail(String email);
}
