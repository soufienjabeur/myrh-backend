package com.myrh.repository;

import com.myrh.domain.Formation;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface de repository pour la gestion des entités Formation.
 * Fournit des méthodes pour interagir avec la table de base de données Formation.
 */
public interface FormationRepository extends JpaRepository<Formation, Long> {

    /**
     * Recherche une formation par son titre.
     *
     * @param titre Le titre de la formation.
     * @return La formation avec le titre spécifié, ou null si elle n'est pas trouvée.
     */
    Formation findByTitre(String titre);
}