package com.myrh.repository;
import com.myrh.domain.Departement;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface de repository pour la gestion des entités Departement.
 * Fournit des méthodes pour interagir avec la table de base de données Departement.
 */
public interface DepartmentRepository extends JpaRepository<Departement, Long> {

    /**
     * Recherche un département par son nom.
     *
     * @param name Le nom du département.
     * @return Le département avec le nom spécifié, ou null s'il n'est pas trouvé.
     */
    Departement findDepartmentByName(String name);
}

