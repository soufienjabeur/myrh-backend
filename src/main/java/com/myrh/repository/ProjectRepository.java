package com.myrh.repository;

import com.myrh.domain.Projet;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for managing Project entities.
 * Provides methods to interact with the Project database table.
 */
public interface ProjectRepository extends JpaRepository<Projet, Long> {

    /**
     * Find a project by its title.
     *
     * @param title Le titre du projet.
     * @return Le projet avec le titre spécifié, ou null s'il n'est pas trouvé.
     */
    Projet findByTitre(String title);
}

