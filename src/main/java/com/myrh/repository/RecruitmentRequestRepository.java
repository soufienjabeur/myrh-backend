package com.myrh.repository;

import com.myrh.domain.DemandeRecrutement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Interface de repository pour la gestion des entités DemandeRecrutement.
 * Fournit des méthodes pour interagir avec la table de base de données DemandeRecrutement.
 */
public interface RecruitmentRequestRepository extends JpaRepository<DemandeRecrutement, Long> {

    /**
     * Recherche une demande de recrutement par son message.
     *
     * @param message Le message de la demande de recrutement.
     * @return La demande de recrutement avec le message spécifié, ou null si elle n'est pas trouvée.
     */
    DemandeRecrutement findByMessage(String message);

    /**
     * Supprime les demandes de recrutement associées à l'identifiant d'un employé.
     *
     * @param employeeId L'identifiant de l'employé pour lequel on souhaite supprimer les demandes de recrutement.
     */
    @Modifying
    @Query("DELETE FROM DemandeRecrutement d WHERE d.employee.id = :employeeId")
    void deleteByEmployeeId(@Param("employeeId") Long employeeId);
}
