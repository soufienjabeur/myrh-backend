package com.myrh.exception.domain;

/**
 * Exception personnalisée représentant le cas où un nom d'utilisateur existe déjà dans le système.
 * Elle étend la classe {@link java.lang.Exception}.
 *
 * Cette exception est généralement utilisée pour signaler une tentative de création ou de modification
 * d'un utilisateur avec un nom d'utilisateur déjà existant dans la base de données.
 *
 * @author Votre Nom
 * @version 1.0
 * @since 2024-02-19
 */
public class UsernameExistException extends Exception {

    /**
     * Constructeur de l'exception avec un message détaillant la raison de l'erreur.
     *
     * @param message Le message détaillant l'erreur.
     */
    public UsernameExistException(String message) {
        super(message);
    }
}

