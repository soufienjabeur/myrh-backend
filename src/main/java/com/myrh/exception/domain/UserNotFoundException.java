package com.myrh.exception.domain;

/**
 * Exception personnalisée représentant le cas où un utilisateur n'est pas trouvé dans le système.
 * Elle étend la classe {@link java.lang.Exception}.
 *
 * Cette exception est généralement utilisée pour signaler une tentative d'accès ou de manipulation d'un utilisateur
 * qui n'existe pas dans la base de données.
 *
 */
public class UserNotFoundException extends Exception {

    /**
     * Constructeur de l'exception avec un message détaillant la raison de l'erreur.
     *
     * @param message Le message détaillant l'erreur.
     */
    public UserNotFoundException(String message) {
        super(message);
    }
}

