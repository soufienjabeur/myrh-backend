package com.myrh.exception.domain;

/**
 * Exception personnalisée représentant le cas où une adresse e-mail n'est pas trouvée dans le système.
 * Elle étend la classe {@link java.lang.Exception}.
 *
 * Cette exception est généralement utilisée pour signaler une tentative d'accès ou de manipulation d'un utilisateur
 * avec une adresse e-mail qui n'existe pas dans la base de données.
 *
 */
public class EmailNotFoundException extends Exception {

    /**
     * Constructeur de l'exception avec un message détaillant la raison de l'erreur.
     *
     * @param message Le message détaillant l'erreur.
     */
    public EmailNotFoundException(String message) {
        super(message);
    }
}
