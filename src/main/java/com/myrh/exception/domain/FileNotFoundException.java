package com.myrh.exception.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Exception personnalisée représentant le cas où un fichier n'est pas trouvé.
 * Elle étend la classe {@link java.lang.RuntimeException} pour indiquer une exception non vérifiée.
 *
 * Cette exception est généralement utilisée pour signaler qu'un fichier recherché n'a pas été trouvé.
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class FileNotFoundException extends RuntimeException {

    /**
     * Message détaillant la raison de l'erreur.
     */
    private String message;
}




