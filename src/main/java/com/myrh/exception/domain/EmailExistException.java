package com.myrh.exception.domain;

/**
 * Exception personnalisée représentant le cas où une adresse e-mail existe déjà dans le système.
 * Elle étend la classe {@link java.lang.Exception}.
 *
 * Cette exception est généralement utilisée pour signaler une tentative de création ou de modification
 * d'un utilisateur avec une adresse e-mail déjà existante dans la base de données.
 *
 */
public class EmailExistException extends Exception {

    /**
     * Constructeur de l'exception avec un message détaillant la raison de l'erreur.
     *
     * @param message Le message détaillant l'erreur.
     */
    public EmailExistException(String message) {
        super(message);
    }
}

