package com.myrh.exception;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.myrh.domain.HttpResponse;
import com.myrh.exception.domain.EmailExistException;
import com.myrh.exception.domain.EmailNotFoundException;
import com.myrh.exception.domain.UserNotFoundException;
import com.myrh.exception.domain.UsernameExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.Objects;

import static org.springframework.http.HttpStatus.*;

/**
 * Gestionnaire d'exceptions global pour l'application. Cette classe est annotée avec
 * {@link org.springframework.web.bind.annotation.RestControllerAdvice} pour fournir des conseils aux contrôleurs
 * REST dans toute l'application. Elle implémente également l'interface {@link ErrorController} pour gérer les erreurs
 * générées par le serveur.
 *
 * La seule responsabilité de cette classe est de définir le chemin d'erreur pour les erreurs génériques de l'application.
 * Le chemin d'erreur est défini par la constante {@code ERROR_PATH} et est utilisé pour rediriger les erreurs HTTP vers
 * un point de terminaison personnalisé.
 *
 */
@RestControllerAdvice
public class ExceptionHandling implements ErrorController {

    /**
     * Chemin d'erreur utilisé pour rediriger les erreurs génériques vers un point de terminaison personnalisé.
     */
    public static final String ERROR_PATH = "/error";

    /**
     * Renvoie le chemin d'erreur défini dans la constante {@code ERROR_PATH}.
     *
     * @return Le chemin d'erreur pour les erreurs génériques de l'application.
     */
    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
