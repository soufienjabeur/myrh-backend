package com.myrh.domain;


import javax.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class OpportuniteRecrutement {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 25)
	private String titre;
	@Column(length = 1000)
	private String description;


	// Define a list of recruitment requests
	@OneToMany(mappedBy = "opportuniteRecrutement", cascade = CascadeType.ALL)
	private List<DemandeRecrutement> demandeRecrutements;

}