package com.myrh.domain;


import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class DemandeConge {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 25)
	private String titre;
	@Column(length = 1000)
	private String cause;

	@JsonFormat(pattern = "MM-dd-yyyy", timezone = "UTC")
    private Date dateDebut;

	@JsonFormat(pattern = "MM-dd-yyyy", timezone = "UTC")
	private Date dateFin;

	@Column(name = "status")
	private Boolean isAccepted ;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	@JsonIgnoreProperties("demandeConges")
	private Employee employee;

}