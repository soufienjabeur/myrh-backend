package com.myrh.domain;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(length = 2500)
	private String name;

	private String documentUrl;

	@ManyToOne
	@JsonIgnoreProperties("documents")
	private Projet projet;
}
