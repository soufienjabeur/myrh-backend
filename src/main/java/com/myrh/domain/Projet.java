package com.myrh.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Projet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 25)
	private String titre;
	@Column(length = 1000)
	private String description;

	@OneToMany(mappedBy = "projet")
	@JsonManagedReference
	private List<Document> documents;


}
