package com.myrh.domain;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class DemandeRecrutement{
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String message;

	private String cvFilePath;

	// Define the relationship with Employee
	@ManyToOne
	@JoinColumn(name = "employee_id")
	@JsonIgnoreProperties("demandeConges")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "opportunite_recrutement_id")
	@JsonIgnoreProperties("demandeRecrutements")
	private OpportuniteRecrutement opportuniteRecrutement;

}