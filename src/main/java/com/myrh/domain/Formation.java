package com.myrh.domain;


import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Formation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 25)
	private String titre;

	@JsonFormat(pattern = "MM-dd-yyyy", timezone = "UTC")
	private Date dateDebut;

	@JsonFormat(pattern = "MM-dd-yyyy", timezone = "UTC")
	private Date dateFin;



}